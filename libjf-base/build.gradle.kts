import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-base")
}

dependencies {
    include(modImplementation(fabricApi.module("fabric-lifecycle-events-v1", prop("fabric_version")))!!)
    shadow("io.gitlab.jfronny:commons:${prop("commons_version")}")
    shadow("io.gitlab.jfronny:commons-gson:${prop("commons_version")}")
    shadow("io.gitlab.jfronny:commons-slf4j:${prop("commons_version")}") {
        isTransitive = false
    }
}
