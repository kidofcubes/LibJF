package io.gitlab.jfronny.libjf.gson;

import io.gitlab.jfronny.gson.GsonBuilder;

public interface GsonAdapter {
    void apply(GsonBuilder builder);
}
