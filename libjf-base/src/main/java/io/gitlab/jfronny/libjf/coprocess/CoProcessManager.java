package io.gitlab.jfronny.libjf.coprocess;

import io.gitlab.jfronny.libjf.LibJf;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.loader.api.FabricLoader;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

public class CoProcessManager implements ModInitializer {
    private final List<CoProcess> coProcesses = new ArrayList<>();
    @Override
    public void onInitialize() {
        coProcesses.addAll(FabricLoader.getInstance().getEntrypoints(LibJf.MOD_ID + ":coprocess", CoProcess.class));
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.SERVER) ServerLifecycleEvents.SERVER_STOPPED.register(server -> this.stop());
        start();
    }

    private void start() {
        for (CoProcess coProcess : coProcesses) {
            coProcess.start();
        }
    }

    private void stop() {
        Iterator<CoProcess> procs = coProcesses.iterator();
        while (procs.hasNext()) {
            CoProcess coProcess = procs.next();
            coProcess.stop();
            if (coProcess instanceof Closeable cl) {
                try {
                    cl.close();
                } catch (IOException e) {
                    LibJf.LOGGER.error("Could not close co-process", e);
                }
            }
            procs.remove();
        }
    }
}
