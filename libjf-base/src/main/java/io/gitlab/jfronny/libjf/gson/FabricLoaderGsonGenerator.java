package io.gitlab.jfronny.libjf.gson;

import io.gitlab.jfronny.gson.*;
import net.fabricmc.loader.api.metadata.CustomValue;

import java.util.Map;

public class FabricLoaderGsonGenerator {
    public static JsonElement toGson(CustomValue customValue) {
        if (customValue == null) return null;
        return switch (customValue.getType()) {
            case OBJECT -> {
                JsonObject jo = new JsonObject();
                for (Map.Entry<String, CustomValue> value : customValue.getAsObject())
                    jo.add(value.getKey(), toGson(value.getValue()));
                yield jo;
            }
            case ARRAY -> {
                JsonArray jo = new JsonArray();
                for (CustomValue value : customValue.getAsArray())
                    jo.add(toGson(value));
                yield jo;
            }
            case STRING -> new JsonPrimitive(customValue.getAsString());
            case NUMBER -> new JsonPrimitive(customValue.getAsNumber());
            case BOOLEAN -> new JsonPrimitive(customValue.getAsBoolean());
            case NULL -> JsonNull.INSTANCE;
        };
    }
}
