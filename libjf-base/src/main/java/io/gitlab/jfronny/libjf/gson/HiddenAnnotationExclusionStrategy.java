package io.gitlab.jfronny.libjf.gson;

import io.gitlab.jfronny.commons.serialize.gson.api.v1.GsonHolders;
import io.gitlab.jfronny.gson.ExclusionStrategy;
import io.gitlab.jfronny.gson.FieldAttributes;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;

public class HiddenAnnotationExclusionStrategy implements ExclusionStrategy {
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT
                ? fieldAttributes.getAnnotation(ServerOnly.class) != null
                : fieldAttributes.getAnnotation(ClientOnly.class) != null;
    }

    public static void register() {
        GsonHolders.modifyBuilder(builder -> builder.setExclusionStrategies(new HiddenAnnotationExclusionStrategy()));
    }
}
