package io.gitlab.jfronny.libjf;

import io.gitlab.jfronny.commons.log.Level;
import io.gitlab.jfronny.commons.log.Logger;
import io.gitlab.jfronny.commons.log.slf4j.SLF4JLogger;
import io.gitlab.jfronny.commons.serialize.gson.api.v1.GsonHolders;
import io.gitlab.jfronny.libjf.gson.HiddenAnnotationExclusionStrategy;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;

public class LibJf implements PreLaunchEntrypoint {
    public static final String MOD_ID = "libjf";
    public static final Logger LOGGER = Logger.forName(MOD_ID);

    static {
        HiddenAnnotationExclusionStrategy.register();
        GsonHolders.registerSerializer();
        Logger.setMinimumLevel(FabricLoader.getInstance().isDevelopmentEnvironment() ? Level.TRACE : Level.INFO);
    }

    @Override
    public void onPreLaunch() {
        setup();
    }

    private static boolean setup = false;
    public static void setup() {
        if (!setup) {
            setup = true;
            Logger.registerFactory(SLF4JLogger::new);
        }
    }
}
