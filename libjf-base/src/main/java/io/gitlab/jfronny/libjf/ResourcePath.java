package io.gitlab.jfronny.libjf;

import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;

public class ResourcePath {
    private final ResourceType type;
    private final Identifier id;

    public ResourcePath(ResourceType type, Identifier id) {
        this.type = type;
        this.id = id;
    }

    public ResourcePath(String name) throws IllegalStateException {
        String[] s1 = name.split("/", 3);
        if (s1.length != 3) {
            throw new IllegalStateException("Could not split path string into resource type and ID due to insufficient length: " + name);
        }
        type = switch (s1[0]) {
            case "assets" -> ResourceType.CLIENT_RESOURCES;
            case "data" -> ResourceType.SERVER_DATA;
            default -> throw new IllegalStateException("Unexpected value for resource type: " + s1[0] + " in: " + name);
        };
        id = new Identifier(s1[1], s1[2]);
    }

    public Identifier getId() {
        return id;
    }

    public ResourceType getType() {
        return type;
    }

    public String getName() {
        return String.format("%s/%s/%s", type.getDirectory(), id.getNamespace(), id.getPath());
    }
}
