import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-config-commands-v1")
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v1"))
    include(modImplementation(fabricApi.module("fabric-command-api-v2", prop("fabric_version")))!!)
}
