import io.gitlab.jfronny.scripts.*

plugins {
    id("jfmod") version "1.3-SNAPSHOT"
}

val nonModSubprojects: List<String> by extra
val devOnlyModules: List<String> by extra

allprojects {
    if (name in nonModSubprojects) return@allprojects

    dependencies {
        modLocalRuntime("com.terraformersmc:modmenu:${prop("modmenu_version")}") {
            exclude("net.fabricmc") // required to work around duplicate fabric loaders
        }
        modLocalRuntime(fabricApi.module("fabric-command-api-v2", prop("fabric_version")))
        compileOnly("io.gitlab.jfronny:commons:${prop("commons_version")}")
        compileOnly("io.gitlab.jfronny:commons-gson:${prop("commons_version")}")
    }
}
