import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-config-core-v1")
}

dependencies {
    api(devProject(":libjf-base"))
    modCompileOnly("com.terraformersmc:modmenu:${prop("modmenu_version")}")
}
