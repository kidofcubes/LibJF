package io.gitlab.jfronny.libjf.config.api.v1;

import java.lang.annotation.*;

/**
 * An annotation for fields in configs or categories which are to be shown
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Entry {
    int width() default 100;
    double min() default Double.NEGATIVE_INFINITY;
    double max() default Double.POSITIVE_INFINITY;
}
