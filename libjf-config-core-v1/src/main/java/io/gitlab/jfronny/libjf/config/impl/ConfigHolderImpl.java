package io.gitlab.jfronny.libjf.config.impl;

import com.google.common.collect.ImmutableMap;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v1.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v1.ConfigInstance;
import net.fabricmc.loader.api.FabricLoader;
import org.jetbrains.annotations.ApiStatus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class ConfigHolderImpl implements ConfigHolder {
    @ApiStatus.Internal
    public static final ConfigHolderImpl INSTANCE = new ConfigHolderImpl();
    private ConfigHolderImpl() {
    }
    public static final String MODULE_ID = "libjf:config";
    private final Map<String, ConfigInstance> configs = new HashMap<>();
    private final Map<Path, ConfigInstance> configsByPath = new HashMap<>();

    @Override
    public void register(String modId, ConfigInstance config) {
        if (isRegistered(modId)) {
            LibJf.LOGGER.warn("Overriding config class of " + modId + " to " + config);
        }
        LibJf.LOGGER.info("Registering config for " + modId);
        configs.put(modId, config);
        config.getFilePath().ifPresent(path -> configsByPath.put(path, config));
    }

    @Override
    public Map<String, ConfigInstance> getRegistered() {
        return ImmutableMap.copyOf(configs);
    }

    @Override
    public ConfigInstance get(String configClass) {
        return configs.get(configClass);
    }

    @Override
    public ConfigInstance get(Path configPath) {
        return configsByPath.get(configPath);
    }

    @Override
    public boolean isRegistered(String modId) {
        return configs.containsKey(modId);
    }

    @Override
    public boolean isRegistered(Path configPath) {
        return configsByPath.containsKey(configPath);
    }

    @Override
    public void migrateFiles(String modId) {
        Path cfg = FabricLoader.getInstance().getConfigDir();
        Path target = cfg.resolve(modId + ".json5");
        if (Files.exists(target)) return;
        if (Files.exists(cfg.resolve(modId + ".json"))) {
            try {
                Files.move(cfg.resolve(modId + ".json"), target);
            } catch (IOException ignored) {
            }
        }
        FabricLoader.getInstance().getModContainer(modId).ifPresent(mod -> {
            try {
                for (String id : mod.getMetadata().getProvides()) {
                    if (Files.exists(cfg.resolve(id + ".json"))) {
                        Files.move(cfg.resolve(id + ".json"), target);
                        return;
                    }
                    if (Files.exists(cfg.resolve(id + ".json5"))) {
                        Files.move(cfg.resolve(id + ".json5"), target);
                        return;
                    }
                }
            } catch (IOException ignored) {
            }
        });
    }
}
