package io.gitlab.jfronny.libjf.config.api.v1;

import java.lang.annotation.*;

/**
 * An annotation for methods which are called every time a config is loaded or changed.
 * Use these to implement additional bounds for values et cetera
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Verifier {
}
