package io.gitlab.jfronny.libjf.config.api.v1;

import java.nio.file.Path;
import java.util.Optional;

/**
 * This class represents a configuration. An instance may be obtained through ConfigHolder or the DSL. Do not implement manually!
 */
public interface ConfigInstance extends ConfigCategory {
    static ConfigInstance get(String modId) {
        return ConfigHolder.getInstance().get(modId);
    }
    void load();
    void write();
    Optional<Path> getFilePath();
}
