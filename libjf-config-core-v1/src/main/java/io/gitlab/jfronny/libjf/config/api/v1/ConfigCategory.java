package io.gitlab.jfronny.libjf.config.api.v1;

import io.gitlab.jfronny.libjf.LibJf;

import java.util.List;
import java.util.Map;

/**
 * This class represents a config category. Do not implement manually!
 */
public interface ConfigCategory {
    String getId();
    String getCategoryPath();
    default String getTranslationPrefix() {
        return getId() + ".jfconfig." + getCategoryPath();
    }
    List<EntryInfo<?>> getEntries();
    Map<String, Runnable> getPresets();
    List<ConfigInstance> getReferencedConfigs();
    Map<String, ConfigCategory> getCategories();
    ConfigInstance getRoot();
    default void fix() {
        for (EntryInfo<?> entry : getEntries()) {
            entry.fix();
        }
    }
    default void reset() {
        for (EntryInfo<?> entry : getEntries()) {
            try {
                entry.reset();
            } catch (IllegalAccessException e) {
                LibJf.LOGGER.error("Could not reload default values", e);
            }
        }
    }
}
