package io.gitlab.jfronny.libjf.config.impl.entrypoint;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v1.JfCustomConfig;
import io.gitlab.jfronny.libjf.config.api.v1.dsl.DSL;
import io.gitlab.jfronny.libjf.config.impl.ConfigHolderImpl;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.entrypoint.EntrypointContainer;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;
import net.minecraft.util.Language;

import java.util.HashSet;
import java.util.Set;
import java.util.function.UnaryOperator;

public class JfConfigSafe implements PreLaunchEntrypoint {
    public static UnaryOperator<String> TRANSLATION_SUPPLIER = s -> null;
    public static final Set<String> REGISTERED_MODS = new HashSet<>();
    @Override
    public void onPreLaunch() {
        LibJf.setup();
        for (EntrypointContainer<Object> custom : FabricLoader.getInstance().getEntrypointContainers(ConfigHolderImpl.MODULE_ID, Object.class)) {
            if (!REGISTERED_MODS.contains(custom.getProvider().getMetadata().getId()) && custom.getEntrypoint() instanceof JfCustomConfig cfg) {
                REGISTERED_MODS.add(custom.getProvider().getMetadata().getId());
                cfg.register(DSL.create(custom.getProvider().getMetadata().getId()));
            }
        }
        TRANSLATION_SUPPLIER = s -> {
            String translated = Language.getInstance().get(s);
            return translated.equals(s) ? null : translated;
        };
    }
}
