package io.gitlab.jfronny.libjf.config.api.v1;

import java.lang.annotation.*;

/**
 * Annotation for config subclasses, which are to be shown as categories
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Category {
    String[] referencedConfigs() default {};
}
