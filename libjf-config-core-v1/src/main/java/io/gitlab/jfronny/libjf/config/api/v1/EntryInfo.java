package io.gitlab.jfronny.libjf.config.api.v1;

import io.gitlab.jfronny.gson.JsonElement;
import io.gitlab.jfronny.gson.stream.JsonWriter;
import io.gitlab.jfronny.libjf.config.api.v1.type.Type;
import io.gitlab.jfronny.libjf.config.impl.dsl.DslEntryInfo;
import org.jetbrains.annotations.ApiStatus;

import java.io.IOException;
import java.lang.reflect.Field;

public interface EntryInfo<T> {
    static EntryInfo<?> ofField(Field field) {
        return DslEntryInfo.ofField(field);
    }

    @ApiStatus.Internal
    static EntryInfo<?> ofField(Class<?> klazz, String name) {
        try {
            return ofField(klazz.getField(name));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get the name of this entry
     * @return This entry's name
     */
    String getName();

    /**
     * @return Get the default value of this entry
     */
    T getDefault();

    /**
     * Gets the current value
     * @return The current value
     */
    T getValue() throws IllegalAccessException;

    /**
     * Set the current value to the parameter
     * @param value The value to use
     */
    void setValue(T value) throws IllegalAccessException;

    /**
     * Get the value type of this entry. Will use the class definition, not the current value
     * @return The type of this entry
     */
    Type getValueType();

    /**
     * Ensure the current value is within expected bounds.
     */
    void fix();

    /**
     * Set this entry's value to that of the element
     * @param element The element to read from
     */
    void loadFromJson(JsonElement element) throws IllegalAccessException;

    /**
     * Write the currently cached value to the writer
     * @param writer The writer to write to
     */
    void writeTo(JsonWriter writer, String translationPrefix) throws IOException, IllegalAccessException;

    /**
     * @return Get the width for this entry
     */
    int getWidth();

    /**
     * @return Get the minimum value of this entry
     */
    double getMinValue();

    /**
     * @return Get the maximum value for this entry
     */
    double getMaxValue();

    default void reset() throws IllegalAccessException {
        setValue(getDefault());
    }
}
