package io.gitlab.jfronny.libjf.config.api.v1;

import io.gitlab.jfronny.libjf.config.api.v1.dsl.DSL;

/**
 * The interface for entrypoints performing custom config registrations
 */
public interface JfCustomConfig {
    void register(DSL.Defaulted dsl);
}
