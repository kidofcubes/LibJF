package io.gitlab.jfronny.libjf.config.api.v1;

import io.gitlab.jfronny.libjf.config.impl.ConfigHolderImpl;

import java.nio.file.Path;
import java.util.Map;

/**
 * An interface for interacting with the class responsible for holding registered configs
 */
public interface ConfigHolder {
    /**
     * Obtain an instance of ConfigHolder with which to interact
     * @return The config holder
     */
    static ConfigHolder getInstance() {
        return ConfigHolderImpl.INSTANCE;
    }

    /**
     * Register a config to a mod
     * @param modId The ID of the mod for which to register the config
     * @param config The config
     */
    void register(String modId, ConfigInstance config);

    /**
     * Get all registered configs by their mod id
     * @return the configs
     */
    Map<String, ConfigInstance> getRegistered();

    /**
     * Obtain a config for a specific mod
     * @param modId The mod to obtain a config for
     * @return The config or null
     */
    ConfigInstance get(String modId);

    /**
     * Obtain a config by its file path
     * @param configPath The file path
     * @return The config or null
     */
    ConfigInstance get(Path configPath);

    /**
     * Check whether this mod has registered a config
     * @param modId The ID of the mod
     * @return Whether a config is registered
     */
    boolean isRegistered(String modId);

    /**
     * Check whether this path belongs to a registered config
     * @param configPath The path to check
     * @return Whether the path belongs to a registered config
     */
    boolean isRegistered(Path configPath);

    /**
     * Migrate config for a mod from old locations (obtained through provides)
     * @param modId The mod to migrate configs for
     */
    void migrateFiles(String modId);
}
