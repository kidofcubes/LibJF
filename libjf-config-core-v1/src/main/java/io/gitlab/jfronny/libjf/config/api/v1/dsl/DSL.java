package io.gitlab.jfronny.libjf.config.api.v1.dsl;

import io.gitlab.jfronny.libjf.config.api.v1.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v1.ConfigInstance;
import io.gitlab.jfronny.libjf.config.impl.dsl.DSLImpl;

/**
 * An interface to allow easy creation or registration of configs
 */
public interface DSL {
    static DSL create() {
        return new DSLImpl();
    }

    static DSL.Defaulted create(String defaultId) {
        return new DSLImpl.Defaulted(defaultId);
    }

    ConfigInstance config(String configId, ConfigBuilder.ConfigBuilderFunction builder);
    ConfigInstance register(String configId, ConfigBuilder.ConfigBuilderFunction builder);
    ConfigInstance register(ConfigHolder config, String configId, ConfigBuilder.ConfigBuilderFunction builder);

    /**
     * A sub-interface of DSL with a default ID and override methods using said ID.
     * Passed to entrypoints, so they don't need to specify their ID in code manually.
     */
    interface Defaulted extends DSL {
        ConfigInstance config(ConfigBuilder.ConfigBuilderFunction builder);
        ConfigInstance register(ConfigBuilder.ConfigBuilderFunction builder);
        ConfigInstance register(ConfigHolder config, ConfigBuilder.ConfigBuilderFunction builder);
    }
}
