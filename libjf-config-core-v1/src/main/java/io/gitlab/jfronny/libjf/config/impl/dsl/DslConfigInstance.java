package io.gitlab.jfronny.libjf.config.impl.dsl;

import io.gitlab.jfronny.libjf.config.api.v1.*;
import io.gitlab.jfronny.libjf.config.api.v1.dsl.CategoryBuilder;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class DslConfigInstance extends DslConfigCategory implements ConfigInstance {
    private final Consumer<ConfigInstance> load;
    private final Consumer<ConfigInstance> write;
    private final Path path;

    public DslConfigInstance(String id,
                             String translationPrefix,
                             List<EntryInfo<?>> entries,
                             Map<String, Consumer<ConfigCategory>> presets,
                             List<Supplier<List<ConfigInstance>>> referencedConfigs,
                             List<CategoryBuilder<?>> categories,
                             Supplier<ConfigInstance> root,
                             List<Consumer<ConfigCategory>> verifiers,
                             Consumer<ConfigInstance> load,
                             Consumer<ConfigInstance> write,
                             @Nullable Path path) {
        super(id, "", translationPrefix, entries, presets, referencedConfigs, categories, root, verifiers);
        this.load = load;
        this.write = write;
        this.path = path;
    }

    @Override
    public void load() {
        load.accept(this);
    }

    @Override
    public void write() {
        write.accept(this);
    }

    @Override
    public Optional<Path> getFilePath() {
        return Optional.ofNullable(path);
    }
}
