package io.gitlab.jfronny.libjf.config.impl.dsl;

import io.gitlab.jfronny.libjf.config.api.v1.*;
import io.gitlab.jfronny.libjf.config.api.v1.dsl.CategoryBuilder;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class DslConfigCategory implements ConfigCategory {
    private final String id;
    private final String categoryPath;
    private final String translationPrefix;
    private final List<EntryInfo<?>> entries;
    private final Map<String, Runnable> presets;
    private final List<Supplier<List<ConfigInstance>>> referencedConfigs;
    private final Map<String, ConfigCategory> categories;
    private final Supplier<ConfigInstance> root;
    private final List<Consumer<ConfigCategory>> verifiers;

    public DslConfigCategory(String id,
                             String categoryPath,
                             String translationPrefix,
                             List<EntryInfo<?>> entries,
                             Map<String, Consumer<ConfigCategory>> presets,
                             List<Supplier<List<ConfigInstance>>> referencedConfigs,
                             List<CategoryBuilder<?>> categories,
                             Supplier<ConfigInstance> root,
                             List<Consumer<ConfigCategory>> verifiers) {
        this.id = id;
        this.categoryPath = categoryPath;
        this.translationPrefix = translationPrefix;
        this.entries = entries;
        this.presets = presets.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey,
                t -> () -> t.getValue().accept(this),
                (u, v) -> v,
                LinkedHashMap::new
        ));
        this.referencedConfigs = referencedConfigs;
        this.categories = categories.stream().collect(Collectors.toMap(
                CategoryBuilder::getId,
                b -> b.build(root),
                (u, v) -> v,
                LinkedHashMap::new
        ));
        this.root = root;
        this.verifiers = verifiers;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getCategoryPath() {
        return categoryPath;
    }

    @Override
    public String getTranslationPrefix() {
        return translationPrefix;
    }

    @Override
    public List<EntryInfo<?>> getEntries() {
        return entries;
    }

    @Override
    public Map<String, Runnable> getPresets() {
        return presets;
    }

    @Override
    public List<ConfigInstance> getReferencedConfigs() {
        return referencedConfigs.stream().map(Supplier::get).<ConfigInstance>mapMulti(Iterable::forEach).toList();
    }

    @Override
    public Map<String, ConfigCategory> getCategories() {
        return categories;
    }

    @Override
    public ConfigInstance getRoot() {
        return root.get();
    }

    @Override
    public void fix() {
        ConfigCategory.super.fix();
        for (Consumer<ConfigCategory> verifier : verifiers) verifier.accept(this);
    }
}
