package io.gitlab.jfronny.libjf.config.api.v1.dsl;

import io.gitlab.jfronny.libjf.config.api.v1.ConfigInstance;

import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * An interface obtained through DSL, used to create config instances
 * @param <Builder> The class implementing this builder
 */
public interface ConfigBuilder<Builder extends ConfigBuilder<Builder>> extends CategoryBuilder<Builder> {
    Builder setLoadMethod(Consumer<ConfigInstance> load);
    Builder setWriteMethod(Consumer<ConfigInstance> write);
    Builder executeAfterWrite(Consumer<ConfigInstance> method);
    Builder setPath(Path path);

    ConfigInstance build();

    @FunctionalInterface
    interface ConfigBuilderFunction {
        ConfigBuilder<?> apply(ConfigBuilder<?> builder);
    }
}
