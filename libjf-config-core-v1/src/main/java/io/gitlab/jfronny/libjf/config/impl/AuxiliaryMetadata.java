package io.gitlab.jfronny.libjf.config.impl;

import io.gitlab.jfronny.commons.serialize.gson.api.v1.GsonHolders;
import io.gitlab.jfronny.libjf.config.api.v1.Category;
import io.gitlab.jfronny.libjf.config.api.v1.JfConfig;
import io.gitlab.jfronny.libjf.config.api.v1.dsl.CategoryBuilder;
import io.gitlab.jfronny.libjf.gson.FabricLoaderGsonGenerator;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.metadata.CustomValue;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

import static io.gitlab.jfronny.libjf.config.impl.ConfigHolderImpl.MODULE_ID;

public class AuxiliaryMetadata {
    public static AuxiliaryMetadata of(Category category) {
        AuxiliaryMetadata meta = new AuxiliaryMetadata();
        if (category != null) {
            meta.referencedConfigs = List.of(category.referencedConfigs());
        }
        return meta.sanitize();
    }

    public static AuxiliaryMetadata of(JfConfig config) {
        AuxiliaryMetadata meta = new AuxiliaryMetadata();
        if (config != null) {
            meta.referencedConfigs = List.of(config.referencedConfigs());
        }
        return meta.sanitize();
    }

    public static @Nullable AuxiliaryMetadata forMod(String modId) {
        var metaRef = new Object() {
            AuxiliaryMetadata meta = null;
        };
        FabricLoader.getInstance().getModContainer(modId).ifPresent(container -> {
            CustomValue cv = container.getMetadata().getCustomValue(MODULE_ID);
            if (cv == null) {
                cv = container.getMetadata().getCustomValue("libjf");
                if (cv != null) {
                    cv = cv.getAsObject().get("config");
                }
            }
            if (cv != null) metaRef.meta = GsonHolders.API.getGson().fromJson(FabricLoaderGsonGenerator.toGson(cv), AuxiliaryMetadata.class);
        });
        return metaRef.meta;
    }

    public List<String> referencedConfigs;

    public void applyTo(CategoryBuilder<?> builder) {
        if (referencedConfigs != null) referencedConfigs.forEach(builder::referenceConfig);
    }

    public AuxiliaryMetadata sanitize() {
        if (referencedConfigs == null) referencedConfigs = List.of();
        else referencedConfigs = List.copyOf(referencedConfigs);
        return this;
    }

    public AuxiliaryMetadata merge(AuxiliaryMetadata other) {
        if (other == null) return this;
        AuxiliaryMetadata meta = new AuxiliaryMetadata();
        meta.referencedConfigs = new LinkedList<>();
        meta.referencedConfigs.addAll(this.referencedConfigs);
        meta.referencedConfigs.addAll(other.referencedConfigs);
        return meta.sanitize();
    }
}
