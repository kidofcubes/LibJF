package io.gitlab.jfronny.libjf.config.api.v1;

import java.lang.annotation.*;

/**
 * An annotation for config classes
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JfConfig {
    String[] referencedConfigs() default {};
}
