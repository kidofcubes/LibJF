package io.gitlab.jfronny.libjf.config.api.v1.dsl;

import io.gitlab.jfronny.libjf.config.api.v1.*;
import io.gitlab.jfronny.libjf.config.api.v1.type.Type;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * An interface obtained through DSL or ConfigBuilder.category(), used for building config categories
 * @param <Builder> The class implementing this builder
 */
public interface CategoryBuilder<Builder extends CategoryBuilder<Builder>> {
    String CONFIG_PRESET_DEFAULT = "libjf-config-v1.default";

    Builder setTranslationPrefix(String translationPrefix);
    String getTranslationPrefix();
    Builder addPreset(String id, Consumer<ConfigCategory> action);
    Builder addPreset(String id, Runnable preset);
    Builder removePreset(String id);
    Builder addVerifier(Consumer<ConfigCategory> verifier);
    Builder addVerifier(Runnable verifier);
    Builder referenceConfig(String id);
    Builder referenceConfig(ConfigInstance config);
    Builder referenceConfig(Supplier<List<ConfigInstance>> gen);
    Builder category(String id, CategoryBuilderFunction builder);
    Builder value(String id, int def, double min, double max, Supplier<Integer> get, Consumer<Integer> set);
    Builder value(String id, long def, double min, double max, Supplier<Long> get, Consumer<Long> set);
    Builder value(String id, float def, double min, double max, Supplier<Float> get, Consumer<Float> set);
    Builder value(String id, double def, double min, double max, Supplier<Double> get, Consumer<Double> set);
    Builder value(String id, String def, Supplier<String> get, Consumer<String> set);
    Builder value(String id, boolean def, Supplier<Boolean> get, Consumer<Boolean> set);
    Builder value(String id, String def, String[] options, Supplier<String> get, Consumer<String> set);
    <T extends Enum<T>> Builder value(String id, T def, Class<T> klazz, Supplier<T> get, Consumer<T> set);
    <T> Builder value(String id, T def, double min, double max, Type type, int width, Supplier<T> get, Consumer<T> set);
    <T> Builder value(EntryInfo<T> entry);

    String getId();

    ConfigCategory build(Supplier<ConfigInstance> root);

    @FunctionalInterface
    interface CategoryBuilderFunction {
        CategoryBuilder<?> apply(CategoryBuilder<?> builder);
    }
}
