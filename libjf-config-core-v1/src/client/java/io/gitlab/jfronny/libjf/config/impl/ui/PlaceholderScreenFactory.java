package io.gitlab.jfronny.libjf.config.impl.ui;

import io.gitlab.jfronny.libjf.config.api.v1.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v1.ui.ConfigScreenFactory;
import net.minecraft.client.gui.screen.Screen;

public class PlaceholderScreenFactory implements ConfigScreenFactory<PlaceholderScreen> {
    @Override
    public PlaceholderScreen create(ConfigInstance config, Screen parent) {
        return new PlaceholderScreen(parent);
    }

    @Override
    public int getPriority() {
        return -100;
    }
}
