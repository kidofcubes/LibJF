package io.gitlab.jfronny.libjf.config.impl.ui;

import io.gitlab.jfronny.libjf.config.api.v1.ui.ConfigScreenFactory;
import net.fabricmc.loader.api.FabricLoader;

import java.util.Comparator;

public class ConfigScreenFactoryDiscovery {
    private static ConfigScreenFactory<?> discovered = null;

    public static ConfigScreenFactory<?> getConfigured() {
        if (discovered == null) {
            discovered = FabricLoader.getInstance()
                    .getEntrypoints("libjf:config_screen", ConfigScreenFactory.class)
                    .stream()
                    .max(Comparator.comparing(ConfigScreenFactory::getPriority))
                    .orElse(new PlaceholderScreenFactory());
        }
        return discovered;
    }
}
