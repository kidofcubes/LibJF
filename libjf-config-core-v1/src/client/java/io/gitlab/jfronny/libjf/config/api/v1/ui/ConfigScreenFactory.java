package io.gitlab.jfronny.libjf.config.api.v1.ui;

import io.gitlab.jfronny.libjf.config.api.v1.ConfigInstance;
import io.gitlab.jfronny.libjf.config.impl.ui.ConfigScreenFactoryDiscovery;
import net.minecraft.client.gui.screen.Screen;

public interface ConfigScreenFactory<S extends Screen> {
    static ConfigScreenFactory<?> getInstance() {
        return ConfigScreenFactoryDiscovery.getConfigured();
    }

    S create(ConfigInstance config, Screen parent);

    int getPriority();
}
