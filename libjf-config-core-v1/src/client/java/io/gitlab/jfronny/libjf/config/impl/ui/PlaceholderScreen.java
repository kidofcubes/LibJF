package io.gitlab.jfronny.libjf.config.impl.ui;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

import java.util.Objects;

public class PlaceholderScreen extends Screen {
    private final Screen parent;
    private final Text description = Text.translatable("libjf-config-core-v1.no-screen.description");

    protected PlaceholderScreen(Screen parent) {
        super(Text.translatable("libjf-config-core-v1.no-screen"));
        this.parent = parent;
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        drawCenteredTextWithShadow(matrices, textRenderer, description, width / 2, (height - textRenderer.fontHeight) / 2, 0xFFFFFF);
        super.render(matrices, mouseX, mouseY, delta);
    }

    @Override
    public void close() {
        Objects.requireNonNull(client).setScreen(parent);
    }
}
