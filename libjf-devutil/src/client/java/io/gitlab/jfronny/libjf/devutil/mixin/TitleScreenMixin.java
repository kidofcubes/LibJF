package io.gitlab.jfronny.libjf.devutil.mixin;

import net.minecraft.client.gui.screen.TitleScreen;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TitleScreen.class)
public class TitleScreenMixin {
    @Mutable @Shadow @Final private boolean doBackgroundFade;

    /**
     * Taken from mod-fungible
     * @author magistermaks
     */
    @Inject(method="<init>(Z)V", at=@At("TAIL"))
    private void init(boolean doBackgroundFade, CallbackInfo ci) {
        this.doBackgroundFade = false;
    }
}
