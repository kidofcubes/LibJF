package io.gitlab.jfronny.libjf.devutil.mixin;

import com.mojang.authlib.minecraft.UserApiService;
import net.minecraft.client.util.ProfileKeys;
import net.minecraft.client.util.Session;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import java.nio.file.Path;

@Mixin(ProfileKeys.class)
public interface ProfileKeysMixin {
    /**
     * @author JFronny
     * @reason Do not load keys!
     */
    @Overwrite
    static ProfileKeys create(UserApiService service, Session session, Path root) {
        return ProfileKeys.MISSING;
    }
}
