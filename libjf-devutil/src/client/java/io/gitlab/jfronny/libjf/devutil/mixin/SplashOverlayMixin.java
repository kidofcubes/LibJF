package io.gitlab.jfronny.libjf.devutil.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.SplashOverlay;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.resource.ResourceReload;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(SplashOverlay.class)
public class SplashOverlayMixin {
    @Shadow @Final private ResourceReload reload;

    /**
     * Taken from mod-fungible
     * @author magistermaks
     */
    @Inject(method="render", at=@At("HEAD"), cancellable=true)
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta, CallbackInfo info) {
        if (reload.isComplete()) {
            MinecraftClient.getInstance().setOverlay(null);
            info.cancel();
        }
    }
}
