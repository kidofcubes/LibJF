package io.gitlab.jfronny.libjf.devutil;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.SharedConstants;

public class DevUtilMain implements ModInitializer {
    @Override
    public void onInitialize() {
        if (!FabricLoader.getInstance().isDevelopmentEnvironment()) {
            throw new RuntimeException("Devutil MUST NOT BE LOADED IN A NON-DEV ENVIRONMENT! If you are a user seeing this message, please contact JFronny!");
        }
        SharedConstants.isDevelopment = true;
    }
}
