import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-devutil")
}

dependencies {
    api(devProject(":libjf-base"))
}
