# libjf-web-v0
libjf-web-v0 provides an HTTP web server you can use in your serverside (and technically also clientside) mods
to serve web content through a unified port.
libjf-web-v0 depends on libjf-config-v1 to provide its config, libjf-base, fabric-lifecycle-events-v1 and fabric-command-api-v1

### Getting started
Implement WebInit and register it as a libjf:web entrypoint. To enable the server, also add the following to your fabric.mod.json:
```json
{
  "custom": {
    "libjf": {
      "web": true
    }
  }
}
```
Please be aware that your entrypoint may be called multiple times during one session.
You can register content providers using the register* methods on the WebServer parameter of your entrypoint.
Please do not store it in a field, you may instead use WebServer.getInstance() if you wish to access it elsewhere.