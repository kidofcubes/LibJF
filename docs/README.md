# About
LibJF is split into several modules, each of which provides separate functionality.
This modularization is inspired by Fabric API.
LibJF is only maintained for the latest version of minecraft and tries to provide a stable ABI during its lifespan
but is open for breaking changes between versions.

Be aware that modules may depend on another and that those transitive dependencies must be JiJd separately.
Modules may also require fabric-api to be present to work properly

### Using LibJF
The recommended way to use LibJF is using the JfMods scripts, which include the LibJF maven repository
and simplify building fabric mods. My own mods (including LibJF) use these.

Otherwise, you can add the repository as follows:
```groovy
repositories {
    maven { url 'https://maven.frohnmeyer-wds.de/artifacts' }
}
```
and include LibJF modules like this:
```groovy
dependencies {
    include modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v1:${project.libjf_version}")
    include modRuntimeOnly("io.gitlab.jfronny.libjf:libjf-config-ui-tiny-v1:${project.libjf_version}")
    include("io.gitlab.jfronny.libjf:libjf-base:${project.libjf_version}")

    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-config-reflect-v1:${project.libjf_version}")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil-v0:${project.libjf_version}")
}
```

For more information on specific modules, you can look at their own pages

### Developing LibJF
To add a LibJF module, create a new directory for it and copy over a build.gradle file from elsewhere,
Reference it in settings.gradle and develop it like any fabric mod.
You should also create a testmod for it (look at the other modules for examples)
JiJing and maven uploads are automatically managed by the JfMods scripts