# About
LibJF Config is a modular config library.
Generally, using a combination of its modules is recommended.
On the following pages, the modules of libjf-config are explained in more detail.
If you just want to get started, a quick setup example for a client-side mod using the compiler plugin can be seen below:

build.gradle:
```groovy
repositories {
    maven { url 'https://maven.frohnmeyer-wds.de/artifacts' }
}

dependencies {
    include modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v1:${project.libjf_version}")
    include modRuntimeOnly("io.gitlab.jfronny.libjf:libjf-config-ui-tiny-v1:${project.libjf_version}")
    include("io.gitlab.jfronny.libjf:libjf-base:${project.libjf_version}")

    annotationProcessor("io.gitlab.jfronny.libjf:libjf-config-compiler-plugin-v2:${project.libjf_version}")
}

loom {
    mods {
        register(name, {
            sourceSet sourceSets.main
        })
    }
}

compileJava {
    options.compilerArgs << "-AmodId=" + archivesName // optionally fill in your mod id explicitly
}
```

fabric.mod.json:
```json
{
  "entrypoints": {
    "libjf:config": ["some.package.JFC_YourConfigClass"] // The JFC_ prefix is for the config-compiler-plugin-v2-generated classes
  }
}
```

Config class:
```java
@JfConfig
public class ConfigExample {
    @Entry public static boolean someEntry = true;
    
    static {
        JFC_ConfigExample.ensureInitialized();
    }
}
```

en_us.json:
```json
{
  "modid.jfconfig.title": "Your Mod",
  "modid.jfconfig.someEntry": "Some Entry",
  "modid.jfconfig.someEntry.tooltip": "Some comment"
}
```

I also recommend adding [ModMenu](https://github.com/TerraformersMC/ModMenu/wiki/API) in order to more easily test your config in dev.