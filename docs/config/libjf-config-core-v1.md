# libjf-config-core-v1
The core module contains the abstractions and annotations used by other modules to interact with configs.
It also contains the code for registering configs to mods, serialization and automatic reloads on change.

## DSL
The DSL is used to create or register instances of ConfigInstance.
You may obtain an instance via `DSL.create()`

## ConfigHolder
You can use this class to obtain your registered config instance or those of other mods.
To do so, use `ConfigHolder.getInstance().get("modId")` and `ConfigHolder.getInstance().getRegistered()`

## Annotations
This module also provides the annotations used in other modules.
Use them as follows:

```java
@JfConfig // This class is a config. Also takes the optional parameter referencedConfigs to add a button to go to the config of another mod
class SomeConfig {
    @Entry // This field is an entry. Also takes the optional parameters min and max for clamping number values
    public static int someEntry = 15; // Any type can be used, but only int, long, float, double, boolean, their Boxes, Strings and enums are supported in ui-tiny
    
    @Preset // This method is a preset. A UI entry for it will be created. When it is pressed, this method will be called
    public static void somePreset() { // Must return void and take no arguments. The method name will be used as the preset name
        someEntry = 12; // You may modify the entries of the config here
    }
    
    @Verifier // This method is a verifier. It will be called whenever a config is changed or loaded.
    public static void someVerifier() {
        if (someEntry % 2 != 0) someEntry++; // You can enforce additional constraints on values
    }
    
    @Category // This is a category. Just like JfConfig, you may specify referencedConfigs
    public static class SomeCategory {
        // A category can contain anything supported by the main config class, including fields, presets, verifiers and subcategories
    }
}
```

## Custom config registration
In order to register mods without the compiler plugin or runtime module,
you can use a `libjf:config` entrypoint implementing JfCustomConfig.
As a parameter, you will be provided with a defaulted DSL with which you must interact to do so.

## Comments
For enums, the possible values will automatically be written to the config as comments.
You can add additional comments for entries by registering tooltips in your language file as follows:
```json
{
  "<mod id>.jfconfig.<entry>.tooltip": "Some comment"
}
```
