# libjf-config-ui-tiny-v1
This module provides an automatically registered, TinyConfig-based UI for all mods using libjf-config.
Embedding this is recommended when developing client-side mods.
Manually generating config screens is also possible through `ConfigScreen.create(config, parent)`