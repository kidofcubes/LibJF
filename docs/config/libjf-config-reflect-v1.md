# libjf-config-reflect-v1
This module uses reflection to register configs from files using the annotations as presented [here](./libjf-config-core-v1.md)
In order to register a config, simply add the class annotated as `@JfConfig` to the `libjf:config` entrypoint.
Please be aware that this module may load your config class before the game classes are available.
Using them there WILL result in problems!

Instead of using this implementation, you should use the [compiler plugin](./libjf-config-compiler-plugin-v2.md),
which removes the runtime overhead of this implementation.