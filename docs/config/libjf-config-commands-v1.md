# libjf-config-commands-v1
This serverside module provides commands for modifying configs using this library.
If you are developing a serverside mod, you may wish to include it to enable easier configuration.
The commands are available under `/libjf config`, using auto-complete is recommended.