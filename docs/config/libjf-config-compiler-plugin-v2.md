# libjf-config-compiler-plugin-v2
This annotation processor provides finds classes annotated with `@JfConfig` and produces the necessary code and registration for them.
Using this is the recommended way to use libjf-config.
Please be aware that the restrictions of [the reflection implementation](./libjf-config-reflect-v1.md) in regard to class loading
will also apply to configs using the plugin if it is loaded during runtime.

Please note that your `fabric.mod.json` must reference the generated class, not your original config.
If you plan on accessing your config before `onInitialize`, it is recommended that you call `.ensureInitialized()` on your
generated class from your config classes static initializer as seen in the example.

The code necessary for using this annotation processor is available [here](./)