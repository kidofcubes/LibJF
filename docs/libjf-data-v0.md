# libjf-data-v0
libjf-data-v0 provides two additional tags for use in other mods or datapacks:

- `libjf:shulker_boxes_illegal` prevents items from being placed in shulker boxes (intended for backpacks)
- `libjf:overpowered` makes entities wearing four or more armor items with this tag invincible