# libjf-devutil
LibJF devutil is intended to be used as `modLocalRuntime`.
It marks the running minecraft instance as a development instance (for example, this removes the need for eula.txt)
and disables the UserApi (removing that Yggdrasil error message)
It does not provide any useful functionality to end users