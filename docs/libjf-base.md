# libjf-base
libjf-base is a dependency of all other modules and provides common functionality between them.
It has no dependencies.
It includes:

- a Gson strategy to ignore fields annotated with GsonHidden, ClientOnly and ServerOnly
- LazySupplier, a supplier that caches the result of another supplier to which it delegates
- ThrowingRunnable and ThrowingSupplier, counterparts of their default lambdas which allow exceptions
- a "flags" system to allow dynamically enabling LibJF features through system properties and fabric.mod.json
- a shared logger and Gson instance
- the ResourcePath abstraction to describe locations of resources as simple strings
- the CoProcess system used internally to control threads separate from the game thread
- an HTTP abstraction from Inceptum

All of these are implemented in one reusable class (except the Gson strategy, whose annotation is separate), which should be simple to read