import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-data-v0")
}

dependencies {
    api(devProject(":libjf-base"))
    include(fabricApi.module("fabric-resource-loader-v0", prop("fabric_version")))
}
