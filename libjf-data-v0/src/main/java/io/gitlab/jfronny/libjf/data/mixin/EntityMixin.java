package io.gitlab.jfronny.libjf.data.mixin;

import io.gitlab.jfronny.libjf.data.Tags;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.concurrent.atomic.AtomicInteger;

@Mixin(Entity.class)
public class EntityMixin {
    @Inject(at = @At("HEAD"), method = "isInvulnerable()Z", cancellable = true)
    public void setInvulnerable(CallbackInfoReturnable<Boolean> info) {
        if (libjf$opArmor()) {
            info.setReturnValue(true);
        }
    }

    @Inject(at = @At("HEAD"), method = "isInvulnerableTo(Lnet/minecraft/entity/damage/DamageSource;)Z", cancellable = true)
    public void setInvulnerable(DamageSource source, CallbackInfoReturnable<Boolean> info) {
        if (libjf$opArmor()) {
            info.setReturnValue(true);
        }
    }

    @Inject(at = @At("HEAD"), method = "kill()V", cancellable = true)
    public void setKillable(CallbackInfo info) {
        if (libjf$opArmor()) {
            info.cancel();
        }
    }

    private boolean libjf$opArmor() {
        Entity entity = (Entity) (Object) this;
        AtomicInteger armorCount = new AtomicInteger();
        for (ItemStack s : entity.getArmorItems()) {
            if (s.isIn(Tags.OVERPOWERED))
                armorCount.getAndIncrement();
        }
        return armorCount.get() >= 4;
    }
}
