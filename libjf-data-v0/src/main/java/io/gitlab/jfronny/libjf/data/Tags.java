package io.gitlab.jfronny.libjf.data;

import io.gitlab.jfronny.libjf.LibJf;
import net.minecraft.item.Item;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;

public class Tags {
    public static final TagKey<Item> SHULKER_ILLEGAL = TagKey.of(RegistryKeys.ITEM, new Identifier(LibJf.MOD_ID, "shulker_boxes_illegal"));
    public static final TagKey<Item> OVERPOWERED = TagKey.of(RegistryKeys.ITEM, new Identifier(LibJf.MOD_ID, "overpowered"));
}
