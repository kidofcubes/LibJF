package io.gitlab.jfronny.libjf.data.manipulation.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.data.manipulation.api.UserResourceEvents;
import net.fabricmc.api.ModInitializer;
import net.minecraft.resource.DirectoryResourcePack;

public class TestEntrypoint implements ModInitializer {
    @Override
    public void onInitialize() {
        // This should prevent resource packs from doing anything if my hooks are working and
        UserResourceEvents.OPEN.register((type, id, previous, pack) -> {
            if (pack instanceof DirectoryResourcePack) {
                LibJf.LOGGER.info(pack.getName() + " opened " + type.name() + "/" + id.toString());
                return null;
            }
            return previous;
        });
    }
}
