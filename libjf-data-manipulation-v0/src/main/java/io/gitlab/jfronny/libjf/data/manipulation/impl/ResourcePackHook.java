package io.gitlab.jfronny.libjf.data.manipulation.impl;

import io.gitlab.jfronny.commons.LazySupplier;
import io.gitlab.jfronny.libjf.data.manipulation.api.UserResourceEvents;
import net.minecraft.resource.*;
import net.minecraft.resource.metadata.ResourceMetadataReader;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.ApiStatus;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
@ApiStatus.Internal
public class ResourcePackHook {
    private static final Map<Long, Boolean> disabled = new HashMap<>();

    @ApiStatus.Internal
    public static void setDisabled(boolean disabled) {
        ResourcePackHook.disabled.put(Thread.currentThread().getId(), disabled);
    }

    @ApiStatus.Internal
    public static boolean isDisabled() {
        return disabled.getOrDefault(Thread.currentThread().getId(), false);
    }

    public static InputSupplier<InputStream> hookOpenRoot(InputSupplier<InputStream> value, ResourcePack pack, String[] fileName) {
        if (isDisabled()) return value;
        return UserResourceEvents.OPEN_ROOT.invoker().openRoot(fileName, value, pack);
    }

    public static InputSupplier<InputStream> hookOpen(InputSupplier<InputStream> value, ResourcePack pack, ResourceType type, Identifier id) {
        if (isDisabled()) return value;
        return UserResourceEvents.OPEN.invoker().open(type, id, value, pack);
    }

    public static ResourcePack.ResultConsumer hookFindResources(ResourcePack pack, ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer target) {
        if (isDisabled()) return target;
        return UserResourceEvents.FIND_RESOURCE.invoker().findResources(type, namespace, prefix, target, pack);
    }

    public static <T> T hookParseMetadata(T value, ResourcePack pack, ResourceMetadataReader<T> reader) throws IOException {
        if (isDisabled()) return value;
        return UserResourceEvents.PARSE_METADATA.invoker().parseMetadata(reader, new LazySupplier<>(value), pack);
    }

    public static <T> T hookParseMetadataEx(IOException ex, ResourcePack pack, ResourceMetadataReader<T> reader) throws IOException {
        if (isDisabled()) throw ex;
        try {
            return hookParseMetadata(null, pack, reader);
        } catch (Throwable t) {
            ex.addSuppressed(t);
            throw ex;
        }
    }
}
