package io.gitlab.jfronny.libjf.data.manipulation.impl.mixin;

import com.google.gson.JsonObject;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.data.manipulation.api.RecipeUtil;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import java.util.*;

@Mixin(RecipeManager.class)
public class RecipeManagerMixin {
    @ModifyVariable(method = "apply(Ljava/util/Map;Lnet/minecraft/resource/ResourceManager;Lnet/minecraft/util/profiler/Profiler;)V", at = @At(value = "INVOKE_ASSIGN", target = "Ljava/util/Set;iterator()Ljava/util/Iterator;", ordinal = 0, remap = false))
    private Iterator<Map.Entry<Identifier, JsonObject>> filterIterator(Iterator<Map.Entry<Identifier, JsonObject>> iterator) {
        ArrayList<Map.Entry<Identifier, JsonObject>> replacement = new ArrayList<>();
        while(iterator.hasNext()) {
            Map.Entry<Identifier, JsonObject> cur = iterator.next();
            Identifier recipeId = cur.getKey();

            if (RecipeUtil.isIdBlocked(recipeId)) {
                LibJf.LOGGER.info("Blocking recipe by identifier: " + recipeId);
            } else {
                replacement.add(cur);
            }
        }

        return replacement.iterator();
    }
}
