package io.gitlab.jfronny.libjf.data.manipulation.api;

import io.gitlab.jfronny.commons.LazySupplier;
import io.gitlab.jfronny.commons.throwable.ThrowingRunnable;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.data.manipulation.impl.ResourcePackHook;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.resource.*;
import net.minecraft.resource.metadata.ResourceMetadataReader;
import net.minecraft.util.Identifier;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

@SuppressWarnings("unused")
public class UserResourceEvents {
    public static <TVal, TEx extends Throwable> TVal disable(ThrowingSupplier<TVal, TEx> then) throws TEx {
        try {
            ResourcePackHook.setDisabled(true);
            return then.get();
        }
        finally {
            ResourcePackHook.setDisabled(false);
        }
    }

    public static <TEx extends Throwable> void disable(ThrowingRunnable<TEx> then) throws TEx {
        try {
            ResourcePackHook.setDisabled(true);
            then.run();
        } finally {
            ResourcePackHook.setDisabled(false);
        }
    }

    public static final Event<OpenRoot> OPEN_ROOT = EventFactory.createArrayBacked(OpenRoot.class,
            listeners -> (fileName, previous, pack) -> {
        InputSupplier<InputStream> lazy = previous;
        for (OpenRoot listener : listeners) {
            lazy = listener.openRoot(fileName, lazy, pack);
        }
        return lazy;
    });

    public static final Event<Open> OPEN = EventFactory.createArrayBacked(Open.class,
            listeners -> (type, id, previous, pack) -> {
        InputSupplier<InputStream> lazy = previous;
        for (Open listener : listeners) {
            lazy = listener.open(type, id, lazy, pack);
        }
        return lazy;
    });

    public static final Event<FindResource> FIND_RESOURCE = EventFactory.createArrayBacked(FindResource.class,
            listeners -> (type, namespace, prefix, previous, pack) -> {
        ResourcePack.ResultConsumer lazy = previous;
        for (FindResource listener : listeners) {
            lazy = listener.findResources(type, namespace, prefix, lazy, pack);
        }
        return lazy;
    });

    public static final Event<ParseMetadata> PARSE_METADATA = EventFactory.createArrayBacked(ParseMetadata.class, ParseMetadataHandler::new);

    // Here because generic targets don't work
    private static class ParseMetadataHandler implements ParseMetadata {
        private final ParseMetadata[] listeners;

        public ParseMetadataHandler(ParseMetadata[] listeners) {
            this.listeners = listeners;
        }

        @Override
        public <T> T parseMetadata(ResourceMetadataReader<T> reader, Supplier<T> previous, ResourcePack pack) throws IOException {
            LazySupplier<T> lazy = new LazySupplier<>(previous);
            for (ParseMetadata listener: listeners) {
                lazy = lazy.andThen(supplier -> {
                    try {
                        return listener.parseMetadata(reader, supplier, pack);
                    } catch (IOException e) {
                        LibJf.LOGGER.error("Could not call ResourcePack.OPEN_ROOT listener", e);
                        return null;
                    }
                });
            }
            return lazy.get();
        }
    }

    public interface OpenRoot {
        InputSupplier<InputStream> openRoot(String[] fileName, InputSupplier<InputStream> previous, ResourcePack pack);
    }

    public interface Open {
        InputSupplier<InputStream> open(ResourceType type, Identifier id, InputSupplier<InputStream> previous, ResourcePack pack);
    }

    public interface FindResource {
        ResourcePack.ResultConsumer findResources(ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer previous, ResourcePack pack);
    }

    public interface ParseMetadata {
        <T> T parseMetadata(ResourceMetadataReader<T> reader, Supplier<T> previous, ResourcePack pack) throws IOException;
    }
}
