import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-data-manipulation-v0")
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-unsafe-v0"))
    modApi(fabricApi.module("fabric-api-base", prop("fabric_version")))
}
