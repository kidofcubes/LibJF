import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-config-ui-tiny-v1")
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v1"))
    include(fabricApi.module("fabric-resource-loader-v0", prop("fabric_version")))
}
