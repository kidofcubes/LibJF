package io.gitlab.jfronny.libjf.config.api.v1.ui.tiny;

import io.gitlab.jfronny.libjf.config.api.v1.ConfigInstance;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.TinyConfigScreenFactory;
import net.minecraft.client.gui.screen.Screen;

@Deprecated
public interface ConfigScreen {
    TinyConfigScreenFactory FACTORY = new TinyConfigScreenFactory();
    static Screen create(ConfigInstance config, Screen parent) {
        return FACTORY.create(config, parent);
    }
}
