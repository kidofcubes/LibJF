package io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry;

import net.minecraft.client.gui.widget.SliderWidget;
import net.minecraft.text.Text;

import java.util.Objects;
import java.util.function.Consumer;

public class CustomSlider extends SliderWidget {
    private final double min;
    private final double max;
    private final Consumer<Double> onChange;
    private final boolean wholeNumber;

    public CustomSlider(int x, int y, int width, int height, double value, double min, double max, Consumer<Double> onChange, boolean wholeNumber) {
        super(x, y, width, height, toText(value), rangeToSlider(value, min, max));
        this.min = min;
        this.max = max;
        this.onChange = onChange;
        this.wholeNumber = wholeNumber;
    }

    @Override
    protected void updateMessage() {
        setMessage(toText(getValue()));
    }

    @Override
    protected void applyValue() {
        onChange.accept(getValue());
    }

    public double getValue() {
        double result = sliderToRange(value, min, max);
        if (wholeNumber) result = Math.floor(result + 0.5);
        return result;
    }

    public void setValue(double value) {
        this.value = rangeToSlider(value, min, max);
    }

    private static double sliderToRange(double value, double min, double max) {
        return value * (max - min) + min;
    }

    private static double rangeToSlider(double value, double min, double max) {
        return (value - min) / (max - min);
    }

    private static Text toText(double value) {
        return Text.literal(Objects.toString(value));
    }
}
