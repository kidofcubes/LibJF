package io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.*;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.*;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

import java.sql.Ref;
import java.util.List;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

@Environment(EnvType.CLIENT)
public class EntryListWidget extends ElementListWidget<EntryListWidget.ConfigEntry> {
    TextRenderer textRenderer;

    public EntryListWidget(MinecraftClient client, TextRenderer tr, int width, int height, int top, int bottom, int itemHeight) {
        super(client, width, height, top, bottom, itemHeight);
        this.centerListVertically = false;
        textRenderer = tr;
        setRenderBackground(client.world == null);
    }

    @Override
    public int getScrollbarPositionX() {
        return this.width -7;
    }

    public void addUnknown(ClickableWidget resetButton, BooleanSupplier resetVisible, Text text, Reflowable reflow) {
        this.addEntry(new ConfigUnknownEntry(text, resetVisible, resetButton, reflow));
    }

    public void addButton(@Nullable ClickableWidget button, ClickableWidget resetButton, BooleanSupplier resetVisible, Text text, Reflowable reflow) {
        this.addEntry(new ConfigScreenEntry(button, text, resetVisible, resetButton, reflow));
    }

    public void addReference(Text text, Supplier<Screen> targetScreen) {
        this.addEntry(new ConfigReferenceEntry(width, text, targetScreen));
    }

    @Override
    public int getRowWidth() {
        return 10000;
    }

    public Optional<Text> getHoveredEntryTitle(double mouseY) {
        for (ConfigEntry abstractEntry : this.children()) {
            if (abstractEntry instanceof ConfigScreenEntry entry
                    && entry.button.visible
                    && mouseY >= entry.button.getY() && mouseY < entry.button.getY() + itemHeight) {
                return Optional.of(entry.getText());
            } else if (abstractEntry instanceof ConfigUnknownEntry entry
                    && mouseY >= entry.resetButton.getY() && mouseY < entry.resetButton.getY() + itemHeight) {
                return Optional.of(entry.getText());
            }
        }
        return Optional.empty();
    }

    public void refreshGrid(ScreenRect tabArea) {
        updateSize(tabArea.width(), tabArea.height(), tabArea.getTop(), tabArea.getBottom());
        setLeftPos(tabArea.getLeft());
        for (int i = 0, len = getEntryCount(); i < len; i++) {
            getEntry(i).reflow(width, height);
        }
    }

    @Environment(EnvType.CLIENT)
    public abstract static class ConfigEntry extends Entry<ConfigEntry> implements Reflowable {
        @Override
        public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            if (hovered) {
                fill(matrices, x, y, x + entryWidth, y + entryHeight, 0x24FFFFFF);
            }
        }
    }

    @Environment(EnvType.CLIENT)
    public static class ConfigReferenceEntry extends ConfigEntry {
        private final ClickableWidget button;

        public ConfigReferenceEntry(int width, Text text, Supplier<Screen> targetScreen) {
            this.button = ButtonWidget.builder(text, btn -> MinecraftClient.getInstance().setScreen(targetScreen.get()))
                    .dimensions(width / 2 - 154, 0, 308, 20)
                    .build();
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of(button);
        }

        @Override
        public List<? extends Element> children() {
            return List.of(button);
        }

        @Override
        public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            super.render(matrices, index, y, x, entryWidth, entryHeight, mouseX, mouseY, hovered, tickDelta);
            button.setY(y);
            button.render(matrices, mouseX, mouseY, tickDelta);
        }

        @Override
        public void reflow(int width, int height) {
            button.setX(width / 2 - 154);
        }
    }

    @Environment(EnvType.CLIENT)
    public static class ConfigScreenEntry extends ConfigEntry {
        private static final TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;
        public final ClickableWidget button;
        private final BooleanSupplier resetVisible;
        private final ClickableWidget resetButton;
        private final Text text;
        private final Reflowable reflow;

        public ConfigScreenEntry(ClickableWidget button, Text text, BooleanSupplier resetVisible, ClickableWidget resetButton, Reflowable reflow) {
            this.button = button;
            this.resetVisible = resetVisible;
            this.resetButton = resetButton;
            this.text = text;
            this.reflow = reflow;
        }

        @Override
        public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            super.render(matrices, index, y, x, entryWidth, entryHeight, mouseX, mouseY, hovered, tickDelta);
            button.setY(y);
            button.render(matrices, mouseX, mouseY, tickDelta);
            drawTextWithShadow(matrices,textRenderer, text,12,y+5,0xFFFFFF);
            if (resetVisible.getAsBoolean()) {
                resetButton.setY(y);
                resetButton.render(matrices, mouseX, mouseY, tickDelta);
            }
        }

        @Override
        public void reflow(int width, int height) {
            reflow.reflow(width, height);
        }

        @Override
        public List<? extends Element> children() {
            return List.of(button, resetButton);
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of(button);
        }

        public Text getText() {
            return text;
        }
    }

    @Environment(EnvType.CLIENT)
    public static class ConfigUnknownEntry extends ConfigEntry {
        private static final TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;
        private final BooleanSupplier resetVisible;
        private final ClickableWidget resetButton;
        private final Text text;
        private final Reflowable reflow;

        public ConfigUnknownEntry(Text text, BooleanSupplier resetVisible, ClickableWidget resetButton, Reflowable reflow) {
            this.resetVisible = resetVisible;
            this.resetButton = resetButton;
            this.text = text;
            this.reflow = reflow;
        }

        @Override
        public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
            super.render(matrices, index, y, x, entryWidth, entryHeight, mouseX, mouseY, hovered, tickDelta);
            drawTextWithShadow(matrices,textRenderer, text,12,y+5,0xFFFFFF);
            if (resetVisible.getAsBoolean()) {
                resetButton.setY(y);
                resetButton.render(matrices, mouseX, mouseY, tickDelta);
            }
        }

        @Override
        public void reflow(int width, int height) {
            reflow.reflow(width, height);
        }

        @Override
        public List<? extends Element> children() {
            return List.of(resetButton);
        }

        @Override
        public List<? extends Selectable> selectableChildren() {
            return List.of();
        }

        public Text getText() {
            return text;
        }
    }
}
