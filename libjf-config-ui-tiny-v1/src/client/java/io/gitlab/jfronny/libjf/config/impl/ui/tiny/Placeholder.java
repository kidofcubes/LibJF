package io.gitlab.jfronny.libjf.config.impl.ui.tiny;

import net.minecraft.client.gui.*;
import net.minecraft.client.gui.navigation.GuiNavigation;
import net.minecraft.client.gui.navigation.GuiNavigationPath;
import net.minecraft.client.gui.screen.narration.NarrationMessageBuilder;
import net.minecraft.client.util.math.MatrixStack;
import org.jetbrains.annotations.Nullable;

public final class Placeholder<T extends Element & Selectable & Drawable> implements Element, Selectable, Drawable {
    private T child;

    public Placeholder(T child) {
        this.child = child;
    }

    public void setChild(T child) {
        this.child = child;
    }

    public T getChild() {
        return child;
    }

    @Override
    public SelectionType getType() {
        return child.getType();
    }

    @Override
    public boolean isNarratable() {
        return child.isNarratable();
    }

    @Override
    public void appendNarrations(NarrationMessageBuilder builder) {
        child.appendNarrations(builder);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        child.render(matrices, mouseX, mouseY, delta);
    }

    @Override
    public void mouseMoved(double mouseX, double mouseY) {
        child.mouseMoved(mouseX, mouseY);
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        return child.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    public boolean mouseReleased(double mouseX, double mouseY, int button) {
        return child.mouseReleased(mouseX, mouseY, button);
    }

    @Override
    public boolean mouseDragged(double mouseX, double mouseY, int button, double deltaX, double deltaY) {
        return child.mouseDragged(mouseX, mouseY, button, deltaX, deltaY);
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double amount) {
        return child.mouseScrolled(mouseX, mouseY, amount);
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        return child.keyPressed(keyCode, scanCode, modifiers);
    }

    @Override
    public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
        return child.keyReleased(keyCode, scanCode, modifiers);
    }

    @Override
    public boolean charTyped(char chr, int modifiers) {
        return child.charTyped(chr, modifiers);
    }

    @Nullable
    @Override
    public GuiNavigationPath getNavigationPath(GuiNavigation navigation) {
        return child.getNavigationPath(navigation);
    }

    @Override
    public boolean isMouseOver(double mouseX, double mouseY) {
        return child.isMouseOver(mouseX, mouseY);
    }

    @Override
    public void setFocused(boolean focused) {
        child.setFocused(focused);
    }

    @Override
    public boolean isFocused() {
        return child.isFocused();
    }

    @Nullable
    @Override
    public GuiNavigationPath getFocusedPath() {
        return child.getFocusedPath();
    }

    @Override
    public ScreenRect getNavigationFocus() {
        return child.getNavigationFocus();
    }

    @Override
    public int getNavigationOrder() {
        return child.getNavigationOrder();
    }
}
