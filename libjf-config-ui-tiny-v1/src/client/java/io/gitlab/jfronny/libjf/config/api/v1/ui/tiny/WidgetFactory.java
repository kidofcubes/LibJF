package io.gitlab.jfronny.libjf.config.api.v1.ui.tiny;

import io.gitlab.jfronny.libjf.config.impl.ui.tiny.TinyConfigScreen;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry.Reflowable;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.widget.ClickableWidget;

public interface WidgetFactory {
    Widget build(TinyConfigScreen screen, TextRenderer textRenderer);

    record Widget(Runnable updateControls, ClickableWidget control, Reflowable reflow) implements Reflowable {
        @Override
        public void reflow(int width, int height) {
            reflow.reflow(width, height);
        }
    }
}
