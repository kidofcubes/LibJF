package io.gitlab.jfronny.libjf.config.impl.ui.tiny;

import net.minecraft.client.gui.screen.narration.NarrationMessageBuilder;
import net.minecraft.client.gui.widget.ClickableWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

public class TinyConfigTabWrapper extends ClickableWidget {
    private final TinyConfigTab tab;

    public TinyConfigTabWrapper(TinyConfigTab tab) {
        super(0, 0, 1, 1, Text.literal(""));
        this.tab = tab;
        visible = false;
        active = false;
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderButton(matrices, mouseX, mouseY, delta);
    }

    @Override
    public void renderButton(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        throw new RuntimeException("TinyConfigTabWrapper must not be rendered");
    }

    @Override
    protected void appendClickableNarrations(NarrationMessageBuilder builder) {
        throw new RuntimeException("TinyConfigTabWrapper must not be rendered");
    }

    public TinyConfigTab getTab() {
        return tab;
    }
}
