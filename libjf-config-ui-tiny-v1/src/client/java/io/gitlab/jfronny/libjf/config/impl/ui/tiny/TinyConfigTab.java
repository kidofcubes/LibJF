package io.gitlab.jfronny.libjf.config.impl.ui.tiny;

import io.gitlab.jfronny.libjf.config.api.v1.ConfigCategory;
import io.gitlab.jfronny.libjf.config.api.v1.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v1.ui.tiny.WidgetFactory;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.entry.*;
import io.gitlab.jfronny.libjf.config.impl.ui.tiny.presets.PresetsScreen;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.ScreenRect;
import net.minecraft.client.gui.tab.Tab;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.ClickableWidget;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;

import java.util.*;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class TinyConfigTab implements Tab {
    private final ConfigCategory config;
    private final EntryListWidget list;

    public TinyConfigTab(TinyConfigScreen screen, ConfigCategory config, TextRenderer textRenderer, boolean isRoot) {
        this.config = config;
        List<WidgetState<?>> widgets = EntryInfoWidgetBuilder.buildWidgets(config, screen.widgets);

        config.fix();
        for (WidgetState<?> widget : widgets) {
            widget.updateCache();
        }

        // Sizing is also done in TinyConfigScreen. Keep these in sync!
        this.list = new EntryListWidget(screen.getClient(), textRenderer, screen.width, screen.height, 32, screen.height - 36, 25);

        if (isRoot) {
            for (Map.Entry<String, ConfigCategory> entry : config.getCategories().entrySet()) {
                this.list.addReference(TinyConfigScreen.getTitle(entry.getValue().getTranslationPrefix()),
                        () -> new TinyConfigScreen(entry.getValue(), screen));
            }
        } else {
            if (!config.getPresets().isEmpty()) {
                this.list.addReference(Text.translatable("libjf-config-v1.presets"),
                        () -> new PresetsScreen(screen, config, screen::afterSelectPreset));
            }
        }
        for (WidgetState<?> info : widgets) {
            MutableText name = Text.translatable(config.getTranslationPrefix() + info.entry.getName());
            WidgetFactory.Widget control = info.factory == null ? null : info.factory.build(screen, textRenderer);
            ButtonWidget resetButton = ButtonWidget.builder(Text.translatable("libjf-config-v1.reset"), (button -> {
                        info.reset();
                        if (control != null) control.updateControls().run();
                    }))
                    .dimensions(screen.width - 155, 0, 40, 20)
                    .build();
            BooleanSupplier resetVisible = () -> {
                boolean visible = !Objects.equals(info.entry.getDefault(), info.cachedValue);
                resetButton.active = visible;
                return visible;
            };
            Reflowable reflow = (width, height) -> {
                resetButton.setX(width - 155);
                if (control != null) control.reflow(width, height);
            };
            if (control == null) this.list.addUnknown(resetButton, resetVisible, name, reflow);
            else this.list.addButton(control.control(), resetButton, resetVisible, name, reflow);
        }
        for (ConfigInstance ci : config.getReferencedConfigs()) {
            if (ci != null) {
                this.list.addReference(Text.translatable("libjf-config-v1.see-also", TinyConfigScreen.getTitle(ci.getTranslationPrefix())),
                        () -> new TinyConfigScreen(ci, screen));
            }
        }
    }

    @Override
    public Text getTitle() {
        return TinyConfigScreen.getTitle(config.getTranslationPrefix());
    }

    @Override
    public void forEachChild(Consumer<ClickableWidget> consumer) {
        consumer.accept(new TinyConfigTabWrapper(this));
    }

    @Override
    public void refreshGrid(ScreenRect tabArea) {
        list.refreshGrid(tabArea);
    }

    @Override
    public void tick() {
        Tab.super.tick();
    }

    public EntryListWidget getList() {
        return list;
    }
}
