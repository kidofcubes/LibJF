package io.gitlab.jfronny.libjf.config.impl.ui.tiny.presets;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v1.ConfigCategory;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

import java.util.Map;
import java.util.Objects;

@Environment(EnvType.CLIENT)
public class PresetsScreen extends Screen {
    private final Screen parent;
    private final ConfigCategory config;
    private final Runnable afterSelect;
    private PresetListWidget list;

    public PresetsScreen(Screen parent, ConfigCategory config, Runnable afterSelect) {
        super(Text.translatable("libjf-config-v1.presets"));
        this.parent = parent;
        this.config = config;
        this.afterSelect = afterSelect;
    }

    @Override
    protected void init() {
        super.init();
        this.list = new PresetListWidget(this.client, this.width, this.height, 32, this.height - 32, 25);
        for (Map.Entry<String, Runnable> entry : config.getPresets().entrySet()) {
            this.list.addButton(ButtonWidget.builder(Text.translatable(entry.getKey()),
                    button -> {
                        LibJf.LOGGER.info("Preset selected: " + entry.getKey());
                        entry.getValue().run();
                        config.fix();
                        afterSelect.run();
                        Objects.requireNonNull(client).setScreen(parent);
                    })
                    .dimensions(width / 2 - 100, 0, 200, 20)
                    .build());
        }
        this.addSelectableChild(this.list);
    }

    @Override
    public void close() {
        Objects.requireNonNull(client).setScreen(parent);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        this.renderBackground(matrices);
        this.list.render(matrices, mouseX, mouseY, delta);

        drawCenteredTextWithShadow(matrices, textRenderer, title, width / 2, 16 - textRenderer.fontHeight / 2, 0xFFFFFF);

        super.render(matrices, mouseX, mouseY, delta);
    }
}
