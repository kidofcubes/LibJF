package io.gitlab.jfronny.libjf.translate.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.translate.impl.google.GoogleTranslateService;
import io.gitlab.jfronny.libjf.translate.impl.libretranslate.LibreTranslateService;
import net.fabricmc.api.ModInitializer;

import java.util.Objects;

public class TestEntrypoint implements ModInitializer {
    @Override
    public void onInitialize() {
        try {
            {
                GoogleTranslateService ts = GoogleTranslateService.get();
                LibJf.LOGGER.info("Testing Google Translate");
                final String sourceLA = "Cogito, ergo sum";
                assertEqual("auto", ts.detect(sourceLA).getIdentifier());
                assertEqual("I think, therefore I am", ts.translate(sourceLA, ts.parseLang("la"), ts.parseLang("en")));
            }

            {
                LibreTranslateService ts = LibreTranslateService.get("https://translate.argosopentech.com");
                LibJf.LOGGER.info("Testing LibreTranslate");
                final String sourceEN = "Hello, World!";
                assertEqual("en", ts.detect(sourceEN).getIdentifier());
                assertEqual("Hallo, Welt!", ts.translate(sourceEN, ts.parseLang("en"), ts.parseLang("de")));
            }
        } catch (Throwable e) {
            LibJf.LOGGER.error("Could not verify translation validity", e);
        }
    }

    private static void assertEqual(Object o1, Object o2) {
        if (!Objects.equals(o1, o2))
            throw new AssertionError("Assertion not met: expected " + o1 + " but got " + o2);
    }
}
