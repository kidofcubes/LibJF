package io.gitlab.jfronny.libjf.translate.impl.google;

public record GoogleTranslateLanguage(String name, String id) implements io.gitlab.jfronny.libjf.translate.api.Language {
    public static final GoogleTranslateLanguage AUTO_DETECT = new GoogleTranslateLanguage("Auto-Detect", "auto");

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public String getIdentifier() {
        return id;
    }
}
