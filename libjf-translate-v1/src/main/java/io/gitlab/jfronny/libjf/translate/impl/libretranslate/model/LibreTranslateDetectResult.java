package io.gitlab.jfronny.libjf.translate.impl.libretranslate.model;

public class LibreTranslateDetectResult {
    public float confidence;
    public String language;
}
