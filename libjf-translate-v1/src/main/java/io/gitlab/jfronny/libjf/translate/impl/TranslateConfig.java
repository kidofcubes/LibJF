package io.gitlab.jfronny.libjf.translate.impl;

import io.gitlab.jfronny.libjf.config.api.v1.JfCustomConfig;
import io.gitlab.jfronny.libjf.config.api.v1.dsl.DSL;
import io.gitlab.jfronny.libjf.translate.impl.google.GoogleTranslateService;
import io.gitlab.jfronny.libjf.translate.impl.libretranslate.LibreTranslateService;

public class TranslateConfig implements JfCustomConfig {
    public static String translationService = GoogleTranslateService.NAME;
    public static String libreTranslateHost = "https://translate.argosopentech.com";

    public static void ensureValid() {
        if (translationService == null) translationService = GoogleTranslateService.NAME;
        if (translationService.equals(LibreTranslateService.NAME) && libreTranslateHost == null || libreTranslateHost.isBlank())
            libreTranslateHost = "https://translate.argosopentech.com";
    }

    @Override
    public void register(DSL.Defaulted dsl) {
    }

    static {
        DSL.create("libjf-translate-v1").register(b -> b
                .value("translationService", translationService, new String[]{GoogleTranslateService.NAME, LibreTranslateService.NAME}, () -> translationService, v -> translationService = v) //Hardcoded options to prevent io blocking
                .value("libreTranslateHost", libreTranslateHost, () -> libreTranslateHost, v -> libreTranslateHost = v)
                .addVerifier(TranslateConfig::ensureValid)
        );
    }
}
