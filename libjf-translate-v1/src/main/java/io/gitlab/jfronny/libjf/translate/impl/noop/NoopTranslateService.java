package io.gitlab.jfronny.libjf.translate.impl.noop;

import io.gitlab.jfronny.libjf.translate.api.TranslateException;
import io.gitlab.jfronny.libjf.translate.api.TranslateService;

import java.util.List;

public class NoopTranslateService implements TranslateService<NoopLanguage> {
    public static final String NAME = "Noop";
    public static final NoopTranslateService INSTANCE = new NoopTranslateService();
    private NoopTranslateService() {
    }

    @Override
    public String translate(String textToTranslate, NoopLanguage translateFrom, NoopLanguage translateTo) throws TranslateException {
        return textToTranslate;
    }

    @Override
    public NoopLanguage detect(String text) throws TranslateException {
        return NoopLanguage.INSTANCE;
    }

    @Override
    public NoopLanguage parseLang(String lang) {
        return NoopLanguage.INSTANCE;
    }

    @Override
    public List<NoopLanguage> getAvailableLanguages() {
        return List.of(NoopLanguage.INSTANCE);
    }

    @Override
    public String getName() {
        return NAME;
    }
}
