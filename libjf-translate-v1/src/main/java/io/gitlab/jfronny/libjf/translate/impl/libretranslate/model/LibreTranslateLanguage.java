package io.gitlab.jfronny.libjf.translate.impl.libretranslate.model;

import io.gitlab.jfronny.libjf.translate.api.Language;

public record LibreTranslateLanguage(String code, String name) implements Language {
    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public String getIdentifier() {
        return code;
    }

    public static class ApiResult {
        public String code;
        public String name;

        public LibreTranslateLanguage toLanguage() {
            return new LibreTranslateLanguage(code, name);
        }
    }
}
