package io.gitlab.jfronny.libjf.translate.api;

/**
 * A language for use in TranslateService.translate
 */
public interface Language {
    /**
     * Returns the string to show in UIs for this language
     * @return The display name
     */
    String getDisplayName();

    /**
     * Returns the ID for internal use (like TranslateService.parseLang)
     * @return The ID of this language
     */
    String getIdentifier();
}
