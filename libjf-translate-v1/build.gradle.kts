import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-translate-v1")
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v1"))
}
