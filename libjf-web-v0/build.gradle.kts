import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-web-v0")
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-config-core-v1"))
    include(modImplementation(fabricApi.module("fabric-command-api-v2", prop("fabric_version")))!!)

    annotationProcessor(project(":libjf-config-compiler-plugin-v2"))
}

tasks.compileJava {
    options.compilerArgs.add("-AmodId=" + base.archivesName.get())
}
