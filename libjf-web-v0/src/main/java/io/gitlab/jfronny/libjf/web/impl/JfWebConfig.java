package io.gitlab.jfronny.libjf.web.impl;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v1.*;

import java.io.IOException;
import java.net.ServerSocket;

@JfConfig
public class JfWebConfig {
    @Entry public static String serverIp = "http://127.0.0.1";
    @Entry(min = 0, max = 35535) public static int port = 0;
    @Entry(min = -1, max = 35535) public static int portOverride = -1;
    @Entry(min = 8, max = 64) public static int maxConnections = 20;
    @Entry public static boolean enableFileHost = false;

    public static void ensureValidPort() {
        if (port == 0) {
            try (ServerSocket socket = new ServerSocket(0)) {
                port = socket.getLocalPort();
            } catch (IOException e) {
                LibJf.LOGGER.error("Could not bind port to identify available", e);
            }
            ConfigHolder.getInstance().getRegistered().get("libjf-web-v0").write();
        }
    }

    static {
        JFC_JfWebConfig.ensureInitialized();
    }
}
