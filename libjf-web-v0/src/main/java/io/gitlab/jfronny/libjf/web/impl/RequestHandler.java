package io.gitlab.jfronny.libjf.web.impl;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.web.api.AdvancedSubServer;
import io.gitlab.jfronny.libjf.web.api.ContentProvider;
import io.gitlab.jfronny.libjf.web.impl.util.WebPaths;
import io.gitlab.jfronny.libjf.web.impl.util.bluemapcore.*;

import java.util.*;

public class RequestHandler implements HttpRequestHandler {
    public Map<String, AdvancedSubServer> subServers = new HashMap<>();
    public Map<String, ContentProvider> contentProviders = new HashMap<>();

    @Override
    public HttpResponse handle(HttpRequest request) {
        HttpResponse resp = null;
        try {
            String webPath = WebPaths.simplify(request.getPath());
            if (webPath.length() == 0)
                webPath = "index.html";
            if (contentProviders.containsKey(webPath)) {
                resp = contentProviders.get(webPath).handle(request);
            }
            else {
                String[] segments = webPath.split("/");
                for (int i = segments.length - 1; i >= 0; i--) {
                    String wp = WebPaths.concat(Arrays.copyOfRange(segments, 0, i));
                    if (subServers.containsKey(wp)) {
                        resp = subServers.get(wp).handle(request, Arrays.copyOfRange(segments, i, segments.length));
                        break;
                    }
                }
                if (resp == null) {
                    resp = new HttpResponse(HttpStatusCode.NOT_FOUND);
                }
            }
        } catch (Throwable e) {
            LibJf.LOGGER.error("Caught error while sending", e);
            resp = new HttpResponse(HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
        if (resp.getHeader("Cache-Control").size() == 0)
            resp.addHeader("Cache-Control", "no-cache");
        if (resp.getHeader("Server").size() == 0)
            resp.addHeader("Server", "LibWeb using BlueMapCore");
        return resp;
    }

    public void clear() {
        subServers.clear();
        contentProviders.clear();
    }
}
