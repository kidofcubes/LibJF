package io.gitlab.jfronny.libjf.web.impl;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.web.api.WebInit;
import io.gitlab.jfronny.libjf.web.api.WebServer;
import net.fabricmc.loader.api.FabricLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DefaultFileHost implements WebInit {
    @Override
    public void register(WebServer api) {
        Path p = FabricLoader.getInstance().getConfigDir().resolve("wwwroot");
        if (!Files.isDirectory(p)) {
            try {
                Files.createDirectory(p);
            } catch (IOException e) {
                LibJf.LOGGER.error("Could not create wwwroot", e);
            }
        }
        try {
            LibJf.LOGGER.info(api.registerDir("/", p, false));
        } catch (IOException e) {
            LibJf.LOGGER.error("Could not register wwwroot", e);
        }
    }
}
