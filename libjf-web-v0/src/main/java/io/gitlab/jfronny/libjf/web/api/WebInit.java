package io.gitlab.jfronny.libjf.web.api;

public interface WebInit {
    void register(WebServer api);
}
