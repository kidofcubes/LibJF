package io.gitlab.jfronny.libjf.web.api;

import io.gitlab.jfronny.libjf.web.impl.JfWeb;

import java.io.IOException;
import java.nio.file.Path;

public interface WebServer {
    String register(String webPath, ContentProvider provider);
    String registerFile(String webPath, Path file, Boolean readOnSend) throws IOException;
    String registerFile(String webPath, byte[] data, String contentType);
    String registerDir(String webPath, Path dir, Boolean readOnSend) throws IOException;
    String registerSubServer(String webPath, SubServer subServer);
    String registerSubServer(String webPath, AdvancedSubServer subServer);
    String getServerRoot();
    void stop();
    void restart();
    boolean isActive();

    static WebServer getInstance() {
        return JfWeb.SERVER;
    }
}
