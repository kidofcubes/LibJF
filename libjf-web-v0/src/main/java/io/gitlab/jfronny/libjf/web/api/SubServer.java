package io.gitlab.jfronny.libjf.web.api;

import io.gitlab.jfronny.libjf.web.impl.util.bluemapcore.HttpRequest;
import io.gitlab.jfronny.libjf.web.impl.util.bluemapcore.HttpResponse;

import java.io.IOException;

public interface SubServer {
    HttpResponse handle(HttpRequest request, String[] segments) throws IOException;
}
