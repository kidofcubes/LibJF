package io.gitlab.jfronny.libjf.web.api;

public interface AdvancedSubServer extends SubServer {
    void onStop();
    void onStart();
}
