package io.gitlab.jfronny.libjf.web.impl.util;

public class WebPaths {
    public static String concat(String s1, String s2) {
        return simplify(s1) + "/" + simplify(s2);
    }

    public static String concat(String[] elements) {
        StringBuilder s = new StringBuilder();
        for (String element : elements) {
            s.append("/").append(element);
        }
        return simplify(s.toString());
    }

    public static String simplify(String s) {
        boolean http = false;
        boolean https = false;
        if (s.startsWith("http://")) {
            http = true;
            s = s.substring(7);
        }
        if (s.startsWith("https://")) {
            https = true;
            s = s.substring(8);
        }

        StringBuilder q = new StringBuilder();
        for (String s1 : simplifyPart(s, false).split("/")) {
            String w = simplifyPart(s1, true);
            if (w != null && w.length() != 0)
                q.append("/").append(w);
        }
        String result = simplifyPart(q.toString(), false);
        if (http) result = "http://" + result;
        if (https) result = "https://" + result;
        return result;
    }

    private static String simplifyPart(String s, boolean alpha) {
        String path = s.toLowerCase();
        while (path.startsWith("/")) path = path.substring(1);
        while (path.endsWith("/")) path = path.substring(0, path.length() - 1);
        while (path.startsWith(".")) path = path.substring(1);
        while (path.endsWith(".")) path = path.substring(0, path.length() - 1);
        if (alpha)
            path = path.replaceAll("[^A-Za-z0-9.:]", "");
        return path;
    }
}
