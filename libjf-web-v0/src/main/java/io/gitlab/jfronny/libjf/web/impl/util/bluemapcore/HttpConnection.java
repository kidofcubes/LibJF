/*
 * This file is part of BlueMap, licensed under the MIT License (MIT).
 *
 * Copyright (c) Blue (Lukas Rieger) <https://bluecolored.de>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.gitlab.jfronny.libjf.web.impl.util.bluemapcore;

import io.gitlab.jfronny.libjf.LibJf;

import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class HttpConnection implements Runnable {
	private final HttpRequestHandler handler;
	private final ServerSocket server;
	private final Socket connection;
	private final InputStream in;
	private final OutputStream out;

	public HttpConnection(ServerSocket server, Socket connection, HttpRequestHandler handler, int timeout, TimeUnit timeoutUnit) throws IOException {
		this.server = server;
		this.connection = connection;
		this.handler = handler;

		if (isClosed()){
			throw new IOException("Socket already closed!");
		}

		connection.setSoTimeout((int) timeoutUnit.toMillis(timeout));

		in = this.connection.getInputStream();
		out = this.connection.getOutputStream();
	}

	@Override
	public void run() {
		while (!isClosed() && !server.isClosed()){
			try {
				HttpRequest request = acceptRequest();
				HttpResponse response = handler.handle(request);
				sendResponse(response);
			} catch (InvalidRequestException e){
				try {
					sendResponse(new HttpResponse(HttpStatusCode.BAD_REQUEST));
				} catch (IOException e1) {}
				break;
			} catch (SocketTimeoutException | SocketException | ConnectionClosedException e) {
				break;
			} catch (IOException e) {
				LibJf.LOGGER.error("Unexpected error while processing a HttpRequest!", e);
				break;
			}
		}

		try {
			close();
		} catch (IOException e){
			LibJf.LOGGER.error("Error while closing HttpConnection!", e);
		}
	}

	private void log(HttpRequest request, HttpResponse response) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		LibJf.LOGGER.info(
				connection.getInetAddress().toString()
						+ " [ "
						+ dateFormat.format(date)
						+ " ] \""
						+ request.getMethod()
						+ " " + request.getPath()
						+ " " + request.getVersion()
						+ "\" "
						+ response.getStatusCode().toString());
	}

	private void sendResponse(HttpResponse response) throws IOException {
		response.write(out);
		out.flush();
	}

	private HttpRequest acceptRequest() throws IOException {
		return HttpRequest.read(in);
	}

	public boolean isClosed(){
		return !connection.isBound() || connection.isClosed() || !connection.isConnected() || connection.isOutputShutdown() || connection.isInputShutdown();
	}

	public void close() throws IOException {
		try {
			in.close();
		} finally {
			try {
				out.close();
			} finally {
				connection.close();
			}
		}
	}

	public static class ConnectionClosedException extends IOException {
		private static final long serialVersionUID = 1L;
	}

	public static class InvalidRequestException extends IOException {
		private static final long serialVersionUID = 1L;
	}
}
