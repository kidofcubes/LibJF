package io.gitlab.jfronny.libjf.web.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.web.api.WebInit;
import io.gitlab.jfronny.libjf.web.api.WebServer;
import io.gitlab.jfronny.libjf.web.impl.util.bluemapcore.HttpResponse;
import io.gitlab.jfronny.libjf.web.impl.util.bluemapcore.HttpStatusCode;
import net.fabricmc.loader.api.FabricLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class WebTest implements WebInit {
    @Override
    public void register(WebServer api) {
        Path sourcePath = FabricLoader.getInstance().getModContainer("libjf-web-v0-testmod").get().findPath("test.html").get();
        LibJf.LOGGER.info(api.register("/test/0.html", request -> new HttpResponse(HttpStatusCode.OK).setData(Files.readString(sourcePath))));
        try {
            LibJf.LOGGER.info(api.registerFile("/test/1.html", sourcePath, false));
            LibJf.LOGGER.info(api.registerFile("/test/2.html", sourcePath, true));
        } catch (IOException e) {
            throw new RuntimeException("Could not register hosted files", e);
        }
    }
}
