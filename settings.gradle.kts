pluginManagement {
    repositories {
        maven("https://maven.fabricmc.net/") // FabricMC
        maven("https://maven.frohnmeyer-wds.de/artifacts") // scripts
        gradlePluginPortal()
    }
}

rootProject.name = "libjf"

include("libjf-base")

include("libjf-config-core-v1")
include("libjf-config-reflect-v1")
include("libjf-config-commands-v1")
include("libjf-config-ui-tiny-v1")
include("libjf-data-v0")
include("libjf-data-manipulation-v0")
include("libjf-devutil")
include("libjf-translate-v1")
include("libjf-unsafe-v0")
include("libjf-web-v0")

include("libjf-config-compiler-plugin")
include("libjf-config-compiler-plugin-v2")
