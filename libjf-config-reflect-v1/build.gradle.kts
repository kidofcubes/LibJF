import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-config-reflect-v1")
}

dependencies {
    api(devProject(":libjf-base"))
    api(devProject(":libjf-unsafe-v0"))
    api(devProject(":libjf-config-core-v1"))
}
