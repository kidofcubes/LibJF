package io.gitlab.jfronny.libjf.config.impl.reflect.entrypoint;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.impl.ConfigHolderImpl;
import io.gitlab.jfronny.libjf.unsafe.DynamicEntry;
import io.gitlab.jfronny.libjf.unsafe.UltraEarlyInit;

public class JfConfigUnsafe implements UltraEarlyInit {
    @Override
    public void init() {
        DynamicEntry.execute(ConfigHolderImpl.MODULE_ID, Object.class,
                s -> JfConfigReflectSafe.registerIfMissing(s.modId(), s.instance())
        );
        LibJf.LOGGER.info("Finished LibJF config entrypoint");
    }
}
