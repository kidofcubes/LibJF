package io.gitlab.jfronny.libjf.config.impl.reflect.entrypoint;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v1.*;
import io.gitlab.jfronny.libjf.config.api.v1.dsl.DSL;
import io.gitlab.jfronny.libjf.config.api.v1.reflect.ReflectiveConfigBuilder;
import io.gitlab.jfronny.libjf.config.impl.ConfigHolderImpl;
import io.gitlab.jfronny.libjf.config.impl.entrypoint.JfConfigSafe;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.entrypoint.EntrypointContainer;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;

public class JfConfigReflectSafe implements PreLaunchEntrypoint {
    @Override
    public void onPreLaunch() {
        LibJf.setup();
        for (EntrypointContainer<Object> config : FabricLoader.getInstance().getEntrypointContainers(ConfigHolderImpl.MODULE_ID, Object.class)) {
            registerIfMissing(config.getProvider().getMetadata().getId(), config.getEntrypoint());
        }
    }

    public static void registerIfMissing(String modId, Object config) {
        if (!JfConfigSafe.REGISTERED_MODS.contains(modId)) {
            JfConfigSafe.REGISTERED_MODS.add(modId);
            ConfigHolder.getInstance().migrateFiles(modId);
            if (config instanceof JfCustomConfig cfg) {
                cfg.register(DSL.create(modId));
            } else {
                Class<?> klazz = config.getClass();
                if (klazz.isAnnotationPresent(JfConfig.class)) {
                    DSL.create(modId).register(ReflectiveConfigBuilder.of(modId, klazz));
                } else {
                    LibJf.LOGGER.error("Attempted to register improper config for mod " + modId + " (missing @JfConfig annotation or JfCustomConfig interface)");
                }
            }
        }
    }
}
