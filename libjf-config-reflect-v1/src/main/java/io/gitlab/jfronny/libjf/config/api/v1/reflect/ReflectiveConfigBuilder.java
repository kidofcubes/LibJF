package io.gitlab.jfronny.libjf.config.api.v1.reflect;

import io.gitlab.jfronny.libjf.config.api.v1.dsl.ConfigBuilder;
import io.gitlab.jfronny.libjf.config.impl.reflect.ReflectiveConfigBuilderImpl;

public interface ReflectiveConfigBuilder extends ConfigBuilder.ConfigBuilderFunction {
    static ReflectiveConfigBuilder of(String id, Class<?> klazz) {
        return new ReflectiveConfigBuilderImpl(id, klazz);
    }
}
