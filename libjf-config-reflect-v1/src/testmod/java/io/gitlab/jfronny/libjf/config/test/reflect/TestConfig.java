package io.gitlab.jfronny.libjf.config.test.reflect;

import io.gitlab.jfronny.commons.serialize.gson.api.v1.Ignore;
import io.gitlab.jfronny.libjf.config.api.v1.*;

import java.util.ArrayList;
import java.util.List;

@JfConfig(referencedConfigs = {"libjf-web-v0"})
public class TestConfig {
    @Entry public static boolean disablePacks = false;
    @Entry public static Boolean disablePacks2 = false;
    @Entry public static int intTest = 20;
    @Entry(min = -5, max = 12) public static int intTestB = 20;
    @Entry(min = -6) public static float floatTest = -5;
    @Entry(min = 2, max = 21) public static double doubleTest = 20;
    @Entry public static String dieStr = "lolz";
    @Entry @Ignore public static String guiOnlyStr = "lolz";
    public static String gsonOnlyStr = "lolz";
    @Entry public static Test enumTest = Test.Test;
    @Entry public static List<String> stringList;

    @Preset
    public static void moskau() {
        disablePacks = true;
        disablePacks2 = true;
        intTest = -5;
        floatTest = -6;
        doubleTest = 4;
        dieStr = "Moskau";
    }

    @Verifier
    public static void setIntTestIfDisable() {
        if (disablePacks) intTest = 0;
    }

    @Verifier
    public static void stringListVerifier() {
        if (stringList == null) stringList = new ArrayList<>(List.of("Obama"));
    }

    public enum Test {
        Test, ER
    }

    @Category
    public static class Subcategory {
        @Entry public static boolean boolInSub = false;
        @Entry public static int intIbSub = 15;

        @Category
        public static class Inception {
            @Entry public static Test yesEnum = Test.ER;
        }
    }
}
