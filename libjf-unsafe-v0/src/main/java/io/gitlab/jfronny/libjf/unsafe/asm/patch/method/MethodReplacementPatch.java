package io.gitlab.jfronny.libjf.unsafe.asm.patch.method;

import io.gitlab.jfronny.libjf.unsafe.asm.patch.MethodPatch;
import org.objectweb.asm.tree.*;

public class MethodReplacementPatch implements MethodPatch {
    private final InsnList instructions;
    public MethodReplacementPatch(InsnList instructions) {
        this.instructions = instructions;
    }

    @Override
    public void apply(MethodNode method, ClassNode klazz) {
        method.instructions.clear();
        InsnList clone = new InsnList();
        for (AbstractInsnNode instruction : instructions) {
            clone.add(instruction);
        }
        method.instructions.insert(clone);
    }
}
