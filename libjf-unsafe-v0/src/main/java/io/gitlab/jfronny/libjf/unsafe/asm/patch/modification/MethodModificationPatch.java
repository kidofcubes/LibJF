package io.gitlab.jfronny.libjf.unsafe.asm.patch.modification;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.unsafe.asm.AsmTransformer;
import io.gitlab.jfronny.libjf.unsafe.asm.patch.*;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.util.*;

public class MethodModificationPatch implements Patch {
    private final Map<Key, MethodPatch> patches = new LinkedHashMap<>();

    public MethodModificationPatch(String targetMethodOwner, Set<MethodDescriptorPatch> patches) {
        for (MethodDescriptorPatch patch : patches) {
            this.patches.put(
                    new Key(PatchUtil.mapMethodName(targetMethodOwner, patch.methodName, patch.methodDescriptor),
                            PatchUtil.mapDescriptor(patch.methodDescriptor)),
                    patch.patch
            );
        }
        for (Key s : this.patches.keySet()) {
            if (AsmTransformer.INSTANCE.debugLogsEnabled()) LibJf.LOGGER.info("Registered patch for " + s);
        }
    }

    @Override
    public void apply(ClassNode klazz) {
        for (MethodNode method : klazz.methods) {
            MethodPatch mp = patches.get(new Key(method.name, method.desc));
            if (mp != null) {
                if (AsmTransformer.INSTANCE.debugLogsEnabled()) LibJf.LOGGER.info("Patching " + method.name);
                mp.apply(method, klazz);
            }
        }
    }

    public record MethodDescriptorPatch(String methodName, String methodDescriptor, MethodPatch patch) {
    }

    private record Key(String name, String descriptor) {
        @Override
        public String toString() {
            return name + descriptor;
        }
    }
}
