package io.gitlab.jfronny.libjf.unsafe;

import io.gitlab.jfronny.commons.log.Logger;
import net.fabricmc.loader.impl.util.log.Log;
import net.fabricmc.loader.impl.util.log.LogCategory;

public class FLLogger implements Logger {
    private final LogCategory category;

    public FLLogger(String context, String... names) {
        this.category = LogCategory.createCustom(context, names);
    }

    @Override
    public String getName() {
        return category.name;
    }

    @Override
    public void trace(String msg) {
        Log.trace(category, msg);
    }

    @Override
    public void trace(String format, Object arg) {
        Log.trace(category, format, arg);
    }

    @Override
    public void trace(String format, Object... args) {
        Log.trace(category, format, args);
    }

    @Override
    public void trace(String msg, Throwable t) {
        Log.trace(category, msg, t);
    }

    @Override
    public void debug(String msg) {
        Log.debug(category, msg);
    }

    @Override
    public void debug(String format, Object arg) {
        Log.debug(category, format, arg);
    }

    @Override
    public void debug(String format, Object... args) {
        Log.debug(category, format, args);
    }

    @Override
    public void debug(String msg, Throwable t) {
        Log.debug(category, msg, t);
    }

    @Override
    public void info(String msg) {
        Log.info(category, msg);
    }

    @Override
    public void info(String format, Object arg) {
        Log.info(category, format, arg);
    }

    @Override
    public void info(String format, Object... args) {
        Log.info(category, format, args);
    }

    @Override
    public void info(String msg, Throwable t) {
        Log.info(category, msg, t);
    }

    @Override
    public void warn(String msg) {
        Log.warn(category, msg);
    }

    @Override
    public void warn(String format, Object arg) {
        Log.warn(category, format, arg);
    }

    @Override
    public void warn(String format, Object... args) {
        Log.warn(category, format, args);
    }

    @Override
    public void warn(String msg, Throwable t) {
        Log.warn(category, msg, t);
    }

    @Override
    public void error(String msg) {
        Log.error(category, msg);
    }

    @Override
    public void error(String format, Object arg) {
        Log.error(category, format, arg);
    }

    @Override
    public void error(String format, Object... args) {
        Log.error(category, format, args);
    }

    @Override
    public void error(String msg, Throwable t) {
        Log.error(category, msg, t);
    }
}
