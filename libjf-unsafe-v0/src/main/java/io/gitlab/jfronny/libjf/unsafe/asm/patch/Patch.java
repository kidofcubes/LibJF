package io.gitlab.jfronny.libjf.unsafe.asm.patch;

import org.objectweb.asm.tree.ClassNode;

public interface Patch {
    void apply(ClassNode klazz);
}
