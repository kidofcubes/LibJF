package io.gitlab.jfronny.libjf.unsafe.asm.patch;

import io.gitlab.jfronny.libjf.LibJf;
import org.objectweb.asm.tree.*;

public class PatchDebug {
    public static void print(InsnList instructions) {
        StringBuilder sb = new StringBuilder("InsnList:");
        for (AbstractInsnNode node : instructions) {
            sb.append('\n').append(node.getClass().getSimpleName()).append(' ').append(Integer.toHexString(node.getOpcode()));
            if (node instanceof VarInsnNode v) sb.append(' ').append(v.var);
            else if (node instanceof LabelNode v) sb.append(' ').append(v.getLabel().toString());
            else if (node instanceof MethodInsnNode v) sb.append(" L").append(v.owner).append(';').append(v.name).append(v.desc).append(" (I=").append(v.itf).append(')');
        }
        LibJf.LOGGER.debug(sb.toString());
    }
}
