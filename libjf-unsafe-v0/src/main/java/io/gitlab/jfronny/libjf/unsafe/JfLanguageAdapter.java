package io.gitlab.jfronny.libjf.unsafe;

import io.gitlab.jfronny.commons.log.Level;
import io.gitlab.jfronny.commons.log.Logger;
import io.gitlab.jfronny.libjf.LibJf;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.LanguageAdapter;

public class  JfLanguageAdapter implements LanguageAdapter {
    @Override
    public native <T> T create(net.fabricmc.loader.api.ModContainer mod, String value, Class<T> type);

    static {
        Logger.registerFactory(FLLogger::new); // Reset in LibJf entrypoint
        Logger.setMinimumLevel(FabricLoader.getInstance().isDevelopmentEnvironment() ? Level.TRACE : Level.INFO);
        LibJf.LOGGER.info("Starting unsafe init"); // Also ensures LibJF.<clinit> is called and Gson is initialized
        DynamicEntry.execute("libjf:preEarly", UltraEarlyInit.class, s -> s.instance().init());
        DynamicEntry.execute("libjf:early", UltraEarlyInit.class, s -> s.instance().init());
        LibJf.LOGGER.info("LibJF unsafe init completed");
    }
}
