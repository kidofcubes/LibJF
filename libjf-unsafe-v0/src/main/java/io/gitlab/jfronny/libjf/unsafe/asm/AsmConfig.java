package io.gitlab.jfronny.libjf.unsafe.asm;

import io.gitlab.jfronny.libjf.unsafe.asm.patch.Patch;

import java.util.Set;

public interface AsmConfig {
    Set<String> skipClasses();
    Set<Patch> getPatches();
}
