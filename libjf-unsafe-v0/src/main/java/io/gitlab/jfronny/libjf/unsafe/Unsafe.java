package io.gitlab.jfronny.libjf.unsafe;

import io.gitlab.jfronny.commons.throwable.Try;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;

@SuppressWarnings({"unchecked", "unused"})
public class Unsafe {
    public static final sun.misc.Unsafe theUnsafe = Try.orThrow(() -> {
        Field unsafeField = Try.orThrow(() -> sun.misc.Unsafe.class.getDeclaredField("theUnsafe"));
        unsafeField.setAccessible(true);
        return (sun.misc.Unsafe) unsafeField.get(null);
    });

    public static int getInt(Object o, long offset) {
        return theUnsafe.getInt(o, offset);
    }

    public static void putInt(Object o, long offset, int x) {
        theUnsafe.putInt(o, offset, x);
    }

    public static <T> T getObject(Object o, long offset) {
        return (T) theUnsafe.getObject(o, offset);
    }

    public static void putObject(Object o, long offset, Object x) {
        theUnsafe.putObject(o, offset, x);
    }

    public static boolean getBoolean(Object o, long offset) {
        return theUnsafe.getBoolean(o, offset);
    }

    public static void putBoolean(Object o, long offset, boolean x) {
        theUnsafe.putBoolean(o, offset, x);
    }

    public static byte getByte(Object o, long offset) {
        return theUnsafe.getByte(o, offset);
    }

    public static void putByte(Object o, long offset, byte x) {
        theUnsafe.putByte(o, offset, x);
    }

    public static short getShort(Object o, long offset) {
        return theUnsafe.getShort(o, offset);
    }

    public static void putShort(Object o, long offset, short x) {
        theUnsafe.putShort(o, offset, x);
    }

    public static char getChar(Object o, long offset) {
        return theUnsafe.getChar(o, offset);
    }

    public static void putChar(Object o, long offset, char x) {
        theUnsafe.putChar(o, offset, x);
    }

    public static long getLong(Object o, long offset) {
        return theUnsafe.getLong(o, offset);
    }

    public static void putLong(Object o, long offset, long x) {
        theUnsafe.putLong(o, offset, x);
    }

    public static float getFloat(Object o, long offset) {
        return theUnsafe.getFloat(o, offset);
    }

    public static void putFloat(Object o, long offset, float x) {
        theUnsafe.putFloat(o, offset, x);
    }

    public static double getDouble(Object o, long offset) {
        return theUnsafe.getDouble(o, offset);
    }

    public static void putDouble(Object o, long offset, double x) {
        theUnsafe.putDouble(o, offset, x);
    }

    public static byte getByte(long address) {
        return theUnsafe.getByte(address);
    }

    public static void putByte(long address, byte x) {
        theUnsafe.putByte(address, x);
    }

    public static short getShort(long address) {
        return theUnsafe.getShort(address);
    }

    public static void putShort(long address, short x) {
        theUnsafe.putShort(address, x);
    }

    public static char getChar(long address) {
        return theUnsafe.getChar(address);
    }

    public static void putChar(long address, char x) {
        theUnsafe.putChar(address, x);
    }

    public static int getInt(long address) {
        return theUnsafe.getInt(address);
    }

    public static void putInt(long address, int x) {
        theUnsafe.putInt(address, x);
    }

    public static long getLong(long address) {
        return theUnsafe.getLong(address);
    }

    public static void putLong(long address, long x) {
        theUnsafe.putLong(address, x);
    }

    public static float getFloat(long address) {
        return theUnsafe.getFloat(address);
    }

    public static void putFloat(long address, float x) {
        theUnsafe.putFloat(address, x);
    }

    public static double getDouble(long address) {
        return theUnsafe.getDouble(address);
    }

    public static void putDouble(long address, double x) {
        theUnsafe.putDouble(address, x);
    }

    public static long getAddress(long address) {
        return theUnsafe.getAddress(address);
    }

    public static void putAddress(long address, long x) {
        theUnsafe.putAddress(address, x);
    }

    public static long allocateMemory(long bytes) {
        return theUnsafe.allocateMemory(bytes);
    }

    public static long reallocateMemory(long address, long bytes) {
        return theUnsafe.reallocateMemory(address, bytes);
    }

    public static void setMemory(Object o, long offset, long bytes, byte value) {
        theUnsafe.setMemory(o, offset, bytes, value);
    }

    public static void setMemory(long address, long bytes, byte value) {
        theUnsafe.setMemory(address, bytes, value);
    }

    public static void copyMemory(Object srcBase, long srcOffset, Object destBase, long destOffset, long bytes) {
        theUnsafe.copyMemory(srcBase, srcOffset, destBase, destOffset, bytes);
    }

    public static void copyMemory(long srcAddress, long destAddress, long bytes) {
        theUnsafe.copyMemory(srcAddress, destAddress, bytes);
    }

    public static void freeMemory(long address) {
        theUnsafe.freeMemory(address);
    }

    public static int arrayBaseOffset(Class<?> arrayClass) {
        return theUnsafe.arrayBaseOffset(arrayClass);
    }

    public static int arrayIndexScale(Class<?> arrayClass) {
        return theUnsafe.arrayIndexScale(arrayClass);
    }

    public static int addressSize() {
        return theUnsafe.addressSize();
    }

    public static int pageSize() {
        return theUnsafe.pageSize();
    }

    public static <T> T allocateInstance(Class<T> cls) throws InstantiationException {
        return (T) theUnsafe.allocateInstance(cls);
    }

    public static void throwException(Throwable ee) {
        theUnsafe.throwException(ee);
    }

    public static boolean compareAndSwapObject(Object o, long offset, Object expected, Object x) {
        return theUnsafe.compareAndSwapObject(o, offset, expected, x);
    }

    public static boolean compareAndSwapInt(Object o, long offset, int expected, int x) {
        return theUnsafe.compareAndSwapInt(o, offset, expected, x);
    }

    public static boolean compareAndSwapLong(Object o, long offset, long expected, long x) {
        return theUnsafe.compareAndSwapLong(o, offset, expected, x);
    }

    public static <T> T getObjectVolatile(Object o, long offset) {
        return (T) theUnsafe.getObjectVolatile(o, offset);
    }

    public static void putObjectVolatile(Object o, long offset, Object x) {
        theUnsafe.putObjectVolatile(o, offset, x);
    }

    public static int getIntVolatile(Object o, long offset) {
        return theUnsafe.getIntVolatile(o, offset);
    }

    public static void putIntVolatile(Object o, long offset, int x) {
        theUnsafe.putIntVolatile(o, offset, x);
    }

    public static boolean getBooleanVolatile(Object o, long offset) {
        return theUnsafe.getBooleanVolatile(o, offset);
    }

    public static void putBooleanVolatile(Object o, long offset, boolean x) {
        theUnsafe.putBooleanVolatile(o, offset, x);
    }

    public static byte getByteVolatile(Object o, long offset) {
        return theUnsafe.getByteVolatile(o, offset);
    }

    public static void putByteVolatile(Object o, long offset, byte x) {
        theUnsafe.putByteVolatile(o, offset, x);
    }

    public static short getShortVolatile(Object o, long offset) {
        return theUnsafe.getShortVolatile(o, offset);
    }

    public static void putShortVolatile(Object o, long offset, short x) {
        theUnsafe.putShortVolatile(o, offset, x);
    }

    public static char getCharVolatile(Object o, long offset) {
        return theUnsafe.getCharVolatile(o, offset);
    }

    public static void putCharVolatile(Object o, long offset, char x) {
        theUnsafe.putCharVolatile(o, offset, x);
    }

    public static long getLongVolatile(Object o, long offset) {
        return theUnsafe.getLongVolatile(o, offset);
    }

    public static void putLongVolatile(Object o, long offset, long x) {
        theUnsafe.putLongVolatile(o, offset, x);
    }

    public static float getFloatVolatile(Object o, long offset) {
        return theUnsafe.getFloatVolatile(o, offset);
    }

    public static void putFloatVolatile(Object o, long offset, float x) {
        theUnsafe.putFloatVolatile(o, offset, x);
    }

    public static double getDoubleVolatile(Object o, long offset) {
        return theUnsafe.getDoubleVolatile(o, offset);
    }

    public static void putDoubleVolatile(Object o, long offset, double x) {
        theUnsafe.putDoubleVolatile(o, offset, x);
    }

    public static void putOrderedObject(Object o, long offset, Object x) {
        theUnsafe.putOrderedObject(o, offset, x);
    }

    public static void putOrderedInt(Object o, long offset, int x) {
        theUnsafe.putOrderedInt(o, offset, x);
    }

    public static void putOrderedLong(Object o, long offset, long x) {
        theUnsafe.putOrderedLong(o, offset, x);
    }

    public static void unpark(Object thread) {
        theUnsafe.unpark(thread);
    }

    public static void park(boolean isAbsolute, long time) {
        theUnsafe.park(isAbsolute, time);
    }

    public static int getLoadAverage(double[] loadavg, int nelems) {
        return theUnsafe.getLoadAverage(loadavg, nelems);
    }

    public static int getAndAddInt(Object o, long offset, int delta) {
        return theUnsafe.getAndAddInt(o, offset, delta);
    }

    public static long getAndAddLong(Object o, long offset, long delta) {
        return theUnsafe.getAndAddLong(o, offset, delta);
    }

    public static int getAndSetInt(Object o, long offset, int newValue) {
        return theUnsafe.getAndSetInt(o, offset, newValue);
    }

    public static long getAndSetLong(Object o, long offset, long newValue) {
        return theUnsafe.getAndSetLong(o, offset, newValue);
    }

    public static <T> T getAndSetObject(Object o, long offset, T newValue) {
        return (T) theUnsafe.getAndSetObject(o, offset, newValue);
    }

    public static void loadFence() {
        theUnsafe.loadFence();
    }

    public static void storeFence() {
        theUnsafe.storeFence();
    }

    public static void fullFence() {
        theUnsafe.fullFence();
    }

    public static void invokeCleaner(ByteBuffer directBuffer) {
        theUnsafe.invokeCleaner(directBuffer);
    }
}
