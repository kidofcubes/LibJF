package io.gitlab.jfronny.libjf.unsafe.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.unsafe.asm.AsmConfig;
import io.gitlab.jfronny.libjf.unsafe.asm.patch.Patch;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

import java.util.Set;

public class AsmTest implements AsmConfig {
    @Override
    public Set<String> skipClasses() {
        return null;
    }

    @Override
    public Set<Patch> getPatches() {
        return Set.of(klazz -> {
            if (klazz.name.equals("com/mojang/blaze3d/systems/RenderSystem")) {
                for (MethodNode method : klazz.methods) {
                    if (method.name.equals("initRenderThread")) {
                        method.instructions.insert(new MethodInsnNode(Opcodes.INVOKESTATIC, Type.getInternalName(AsmTest.class), "logSuccess", "()V"));
                    }
                }
            }
        });
    }

    public static void logSuccess() {
        LibJf.LOGGER.info("Successfully ASMd into RenderSystem\n" +
                ":::'###:::::'######::'##::::'##:'########:'########::'######::'########:\n" +
                "::'## ##:::'##... ##: ###::'###:... ##..:: ##.....::'##... ##:... ##..::\n" +
                ":'##:. ##:: ##:::..:: ####'####:::: ##:::: ##::::::: ##:::..::::: ##::::\n" +
                "'##:::. ##:. ######:: ## ### ##:::: ##:::: ######:::. ######::::: ##::::\n" +
                " #########::..... ##: ##. #: ##:::: ##:::: ##...:::::..... ##:::: ##::::\n" +
                " ##.... ##:'##::: ##: ##:.:: ##:::: ##:::: ##:::::::'##::: ##:::: ##::::\n" +
                " ##:::: ##:. ######:: ##:::: ##:::: ##:::: ########:. ######::::: ##::::\n" +
                "..:::::..:::......:::..:::::..:::::..:::::........:::......::::::..:::::");
    }
}
