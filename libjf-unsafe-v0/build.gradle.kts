import io.gitlab.jfronny.scripts.*

base {
    archivesName.set("libjf-unsafe-v0")
}

dependencies {
    api(devProject(":libjf-base"))
}
