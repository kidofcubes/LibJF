LibJF is a library for my smaller mods which contains several common components.
Namely it provides:
- a complete config system with UIs, commands, compile-time codegen, presets, verifiers, etc
- a framework for manipulating loaded data and resources
- two new tags (which you can also use in data packs):
  - `libjf:overpowered`: if an entity only wears armor items with this tag, it will become invulnerable
  - `libjf:shulker_boxes_illegal`: items with this tag cannot be placed inside shulker boxes.
    Intended to be used for backpacks or similar items
- utility code for serialization, script loading, co-processes, IO, etc.
- network utilities including a HTTP client and server
- a configurable and extensible translation system supporting various providers
- utilities for early initialization and ASM patching

If a mod is made by me, it is generally safe to assume it requires LibJF (or ships with parts of it)

If you want to use LibJF yourself, you can find documentation [here](https://pages.frohnmeyer-wds.de/JfMods/LibJF/)
