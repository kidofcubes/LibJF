package io.gitlab.jfronny.libjf.config.plugin;

import io.gitlab.jfronny.libjf.config.plugin.util.ZipCompressor;
import org.apache.tools.zip.ZipOutputStream;
import org.gradle.api.GradleException;
import org.gradle.api.internal.file.copy.CopyAction;
import org.gradle.api.internal.file.copy.CopyActionProcessingStream;
import org.gradle.api.tasks.WorkResult;
import org.gradle.api.tasks.WorkResults;
import org.objectweb.asm.Type;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConfigInjectCopyAction implements CopyAction {
    private final File zipFile;
    private final ZipCompressor compressor;
    private final String encoding;
    private final boolean preserveFileTimestamps;
    private final String modId;
    private final Set<Type> knownConfigClasses = new HashSet<>();

    public ConfigInjectCopyAction(File zipFile,
                                  ZipCompressor compressor,
                                  String encoding,
                                  boolean preserveFileTimestamps,
                                  String modId) {
        this.zipFile = zipFile;
        this.compressor = compressor;
        this.encoding = encoding;
        this.preserveFileTimestamps = preserveFileTimestamps;
        this.modId = modId;
    }

    @Override
    public WorkResult execute(CopyActionProcessingStream stream) {
        try (ZipOutputStream zipOutStr = compressor.createArchiveOutputStream(zipFile)) {
            if (encoding != null) {
                zipOutStr.setEncoding(encoding);
            }
            AtomicBoolean fmjFound = new AtomicBoolean(false);
            stream.process(details -> fmjFound.compareAndSet(false,
                    new StreamAction(zipOutStr, zipFile, preserveFileTimestamps, modId, knownConfigClasses)
                            .processFile(details)
            ));
        } catch (IOException e) {
            throw new GradleException("Could not create ZIP " + zipFile, e);
        }
        return WorkResults.didWork(true);
    }
}
