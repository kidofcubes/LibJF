package io.gitlab.jfronny.libjf.config.plugin.asm;

import org.gradle.api.GradleException;
import org.objectweb.asm.AnnotationVisitor;

import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public class AnnotationMetaGatheringVisitor extends AnnotationVisitor {
    private final List<String> referencedConfigs;

    public AnnotationMetaGatheringVisitor(AnnotationVisitor annotationVisitor, List<String> referencedConfigs) {
        super(ASM9, annotationVisitor);
        this.referencedConfigs = referencedConfigs;
    }

    @Override
    public AnnotationVisitor visitArray(String name) {
        return switch (name) {
            case "referencedConfigs" -> new ArrayVisitor<>(super.visitArray(name), referencedConfigs);
            default -> throw new GradleException("Unknown field in category or JfConfig annotation: " + name);
        };
    }

    private static class ArrayVisitor<T> extends AnnotationVisitor {
        private final List<T> target;

        protected ArrayVisitor(AnnotationVisitor annotationVisitor, List<T> target) {
            super(ASM9, annotationVisitor);
            this.target = target;
        }

        @Override
        public void visit(String name, Object value) {
            super.visit(name, value);
            target.add((T) value);
        }
    }
}
