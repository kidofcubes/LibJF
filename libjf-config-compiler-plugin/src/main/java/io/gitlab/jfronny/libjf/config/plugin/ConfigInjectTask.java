package io.gitlab.jfronny.libjf.config.plugin;

import io.gitlab.jfronny.libjf.config.plugin.util.GradleVersionUtil;
import io.gitlab.jfronny.libjf.config.plugin.util.ZipCompressor;
import org.gradle.api.file.DuplicatesStrategy;
import org.gradle.api.internal.file.copy.CopyAction;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.bundling.Jar;

public abstract class ConfigInjectTask extends Jar {
    @Input
    public abstract Property<String> getModId();
    private final GradleVersionUtil versionUtil;

    public ConfigInjectTask() {
        super();
        setDuplicatesStrategy(DuplicatesStrategy.FAIL);
        versionUtil = new GradleVersionUtil(getProject().getGradle().getGradleVersion());
    }

    @Override
    protected CopyAction createCopyAction() {
        return new ConfigInjectCopyAction(
                getArchiveFile().get().getAsFile(),
                getInternalCompressor(),
                this.getMetadataCharset(),
                isPreserveFileTimestamps(),
                getModId().get()
        );
    }

    @Internal
    protected ZipCompressor getInternalCompressor() {
        return versionUtil.getInternalCompressor(getEntryCompression(), this);
    }
}
