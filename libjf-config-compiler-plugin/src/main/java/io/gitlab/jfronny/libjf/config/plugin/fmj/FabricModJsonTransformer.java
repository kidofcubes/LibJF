package io.gitlab.jfronny.libjf.config.plugin.fmj;

import io.gitlab.jfronny.gson.*;
import io.gitlab.jfronny.gson.stream.JsonReader;
import io.gitlab.jfronny.gson.stream.JsonWriter;
import io.gitlab.jfronny.libjf.config.impl.ConfigHolderImpl;
import io.gitlab.jfronny.libjf.config.plugin.asm.ConfigInjectClassTransformer;
import org.objectweb.asm.Type;

import java.util.Iterator;
import java.util.Set;

public class FabricModJsonTransformer {
    private static final Gson INPUT_GSON = new GsonBuilder().setLenient().create();
    private static final Gson OUTPUT_GSON = new GsonBuilder().create();
    private static final String ENTRYPOINTS = "entrypoints";
    private static final String LIBJF_CONFIG = ConfigHolderImpl.MODULE_ID;
    public static void transform(JsonReader reader, JsonWriter writer, Set<Type> configClasses) {
        JsonObject fmj = INPUT_GSON.<JsonElement>fromJson(reader, JsonElement.class).getAsJsonObject();
        if (!fmj.has(ENTRYPOINTS)) fmj.add(ENTRYPOINTS, new JsonObject());
        JsonObject entrypoints = fmj.get(ENTRYPOINTS).getAsJsonObject();
        if (!entrypoints.has(LIBJF_CONFIG)) entrypoints.add(LIBJF_CONFIG, new JsonArray());
        JsonArray libjfConfig = entrypoints.getAsJsonArray(LIBJF_CONFIG).getAsJsonArray();
        for (Type klazz : configClasses) {
            // Remove references to class
            Iterator<JsonElement> each = libjfConfig.iterator();
            while(each.hasNext()) {
                JsonElement element = each.next();
                if (element.isJsonPrimitive() && element.getAsString().equals(klazz.getClassName())) {
                    each.remove();
                }
            }

            // Add reference to init method
            libjfConfig.add(klazz.getClassName() + "::" + ConfigInjectClassTransformer.REGISTER_METHOD);
        }
        OUTPUT_GSON.toJson(fmj, writer);
    }
}
