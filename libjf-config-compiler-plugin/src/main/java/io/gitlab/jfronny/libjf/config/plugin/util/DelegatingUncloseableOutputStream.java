package io.gitlab.jfronny.libjf.config.plugin.util;

import java.io.IOException;
import java.io.OutputStream;

public class DelegatingUncloseableOutputStream extends OutputStream {
    private final OutputStream delegate;

    public DelegatingUncloseableOutputStream(OutputStream delegate) {
        super();
        this.delegate = delegate;
    }

    @Override
    public void write(int i) throws IOException {
        delegate.write(i);
    }

    @Override
    public void write(byte[] b) throws IOException {
        delegate.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        delegate.write(b, off, len);
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() {
    }
}
