package io.gitlab.jfronny.libjf.config.plugin.asm.value;

import io.gitlab.jfronny.libjf.config.api.v1.*;
import io.gitlab.jfronny.libjf.config.api.v1.dsl.*;
import org.objectweb.asm.Type;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class KnownTypes {
    // DSL types
    public static final Type CONFIG_BUILDER_TYPE = Type.getType(ConfigBuilder.class);
    public static final Type CONFIG_BUILDER_FUNCTION_TYPE = Type.getType(ConfigBuilder.ConfigBuilderFunction.class);
    public static final Type CATEGORY_BUILDER_TYPE = Type.getType(CategoryBuilder.class);
    public static final Type CATEGORY_BUILDER_FUNCTION_TYPE = Type.getType(CategoryBuilder.CategoryBuilderFunction.class);
    public static final Type DSL_TYPE = Type.getType(DSL.class);
    public static final Type DSL_DEFAULTED_TYPE = Type.getType(DSL.Defaulted.class);
    public static final Type CONFIG_HOLDER_TYPE = Type.getType(ConfigHolder.class);
    public static final Type CONFIG_INSTANCE_TYPE = Type.getType(ConfigInstance.class);
    public static final Type ENTRY_INFO_TYPE = Type.getType(EntryInfo.class);
    // Boxes
    public static final Type INT_BOX_TYPE = Type.getType(Integer.class);
    public static final Type LONG_BOX_TYPE = Type.getType(Long.class);
    public static final Type FLOAT_BOX_TYPE = Type.getType(Float.class);
    public static final Type DOUBLE_BOX_TYPE = Type.getType(Double.class);
    public static final Type BOOLEAN_BOX_TYPE = Type.getType(Boolean.class);
    public static final Type CHARACTER_BOX_TYPE = Type.getType(Character.class);
    public static final Type SHORT_BOX_TYPE = Type.getType(Short.class);
    public static final Type BYTE_BOX_TYPE = Type.getType(Byte.class);
    // Additional
    public static final Type STRING_TYPE = Type.getType(String.class);
    public static final Type OBJECT_TYPE = Type.getType(Object.class);
    public static final Type RUNNABLE_TYPE = Type.getType(Runnable.class);
    public static final Type SUPPLIER_TYPE = Type.getType(Supplier.class);
    public static final Type CONSUMER_TYPE = Type.getType(Consumer.class);
    public static final Type NUMBER_TYPE = Type.getType(Number.class);
    public static final Type CLASS_TYPE = Type.getType(Class.class);
}
