package io.gitlab.jfronny.libjf.config.plugin.asm;

import io.gitlab.jfronny.libjf.config.api.v1.Entry;
import io.gitlab.jfronny.libjf.config.plugin.asm.value.DiscoveredValue;
import org.objectweb.asm.*;

import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public class FieldMetaGatheringVisitor extends FieldVisitor {
    private final String name;
    private final List<DiscoveredValue> values;
    private final Type type;

    protected FieldMetaGatheringVisitor(FieldVisitor fieldVisitor, String name, List<DiscoveredValue> values, Type type) {
        super(ASM9, fieldVisitor);
        this.name = name;
        this.values = values;
        this.type = type;
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        if (descriptor.equals(Type.getDescriptor(Entry.class))) {
            return new MetaGatheringAnnotationVisitor(super.visitAnnotation(descriptor, visible));
        }
        return super.visitAnnotation(descriptor, visible);
    }

    public class MetaGatheringAnnotationVisitor extends AnnotationVisitor {
        private double min = Double.NEGATIVE_INFINITY;
        private double max = Double.POSITIVE_INFINITY;
        protected MetaGatheringAnnotationVisitor(AnnotationVisitor annotationVisitor) {
            super(ASM9, annotationVisitor);
        }

        @Override
        public void visit(String name, Object value) {
            super.visit(name, value);
            switch (name) {
                case "min" -> {
                    min = (Double)value;
                }
                case "max" -> {
                    max = (Double)value;
                }
                case "width" -> {}
                default -> throw new IllegalArgumentException("Unexpected name in @Entry annotation: " + name);
            }
        }

        @Override
        public void visitEnd() {
            super.visitEnd();
            values.add(new DiscoveredValue(name, min, max, type));
        }
    }
}
