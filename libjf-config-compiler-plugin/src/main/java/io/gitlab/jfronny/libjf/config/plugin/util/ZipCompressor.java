package io.gitlab.jfronny.libjf.config.plugin.util;


import org.apache.tools.zip.ZipOutputStream;
import org.gradle.api.internal.file.archive.compression.ArchiveOutputStreamFactory;

import java.io.File;

public interface ZipCompressor extends ArchiveOutputStreamFactory {
    ZipOutputStream createArchiveOutputStream(File destination);
}
