package io.gitlab.jfronny.libjf.config.plugin.util;

import org.apache.tools.zip.ZipOutputStream;
import org.gradle.api.tasks.bundling.Jar;
import org.gradle.api.tasks.bundling.ZipEntryCompression;
import org.gradle.util.GradleVersion;

public class GradleVersionUtil {
    private final GradleVersion version;

    public GradleVersionUtil(String version) {
        this.version = GradleVersion.version(version);
    }

    public ZipCompressor getInternalCompressor(ZipEntryCompression entryCompression, Jar jar) {
        return switch (entryCompression) {
            case DEFLATED -> new DefaultZipCompressor(jar.isZip64(), ZipOutputStream.DEFLATED);
            case STORED -> new DefaultZipCompressor(jar.isZip64(), ZipOutputStream.STORED);
        };
    }
}
