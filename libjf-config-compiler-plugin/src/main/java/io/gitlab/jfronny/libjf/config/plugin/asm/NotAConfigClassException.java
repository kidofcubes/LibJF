package io.gitlab.jfronny.libjf.config.plugin.asm;

public class NotAConfigClassException extends RuntimeException {
    public NotAConfigClassException() {
        super("This class is not a config class and should not be modified");
    }
}
