package io.gitlab.jfronny.libjf.config.plugin.asm;

import io.gitlab.jfronny.libjf.config.api.v1.Preset;
import io.gitlab.jfronny.libjf.config.api.v1.Verifier;
import org.objectweb.asm.*;

import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public class MethodMetaGatheringVisitor extends MethodVisitor {
    private final String name;
    private final List<String> presets;
    private final List<String> verifiers;
    protected MethodMetaGatheringVisitor(MethodVisitor methodVisitor, String name, List<String> presets, List<String> verifiers) {
        super(ASM9, methodVisitor);
        this.name = name;
        this.presets = presets;
        this.verifiers = verifiers;
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        if (descriptor.equals(Type.getDescriptor(Preset.class))) {
            presets.add(name);
        }
        if (descriptor.equals(Type.getDescriptor(Verifier.class))) {
            verifiers.add(name);
        }
        return super.visitAnnotation(descriptor, visible);
    }
}
