package io.gitlab.jfronny.libjf.config.plugin.asm;

public enum TransformerMode {
    CONFIG_ROOT, CONFIG_CATEGORY, OTHER
}
