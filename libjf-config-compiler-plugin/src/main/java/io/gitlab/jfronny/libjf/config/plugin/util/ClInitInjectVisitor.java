package io.gitlab.jfronny.libjf.config.plugin.util;

import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.*;

public class ClInitInjectVisitor extends MethodVisitor {
    private final String owner;
    private final String name;
    private final String descriptor;

    public ClInitInjectVisitor(MethodVisitor mw, String owner, String name, String descriptor) {
        super(ASM9, mw);
        this.owner = owner;
        this.name = name;
        this.descriptor = descriptor;
    }

    @Override
    public void visitInsn(int opcode) {
        if (opcode == RETURN) {
            super.visitMethodInsn(INVOKESTATIC, owner, name, descriptor, false);
        }
        super.visitInsn(opcode);
    }
}
