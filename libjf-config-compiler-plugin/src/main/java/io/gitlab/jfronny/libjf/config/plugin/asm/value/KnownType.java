package io.gitlab.jfronny.libjf.config.plugin.asm.value;

import org.objectweb.asm.Type;

import java.util.HashMap;
import java.util.Map;

import static io.gitlab.jfronny.libjf.config.plugin.asm.value.KnownTypes.*;
import static org.objectweb.asm.Type.*;

public enum KnownType {
    INT(INT_BOX_TYPE, INT_TYPE),
    LONG(LONG_BOX_TYPE, LONG_TYPE),
    FLOAT(FLOAT_BOX_TYPE, FLOAT_TYPE),
    DOUBLE(DOUBLE_BOX_TYPE, DOUBLE_TYPE),
    STRING(STRING_TYPE, STRING_TYPE),
    BOOLEAN(BOOLEAN_BOX_TYPE, BOOLEAN_TYPE),
    OBJECT(OBJECT_TYPE, OBJECT_TYPE);

    public final Type boxed;
    public final Type unboxed;
    private static final Map<Type, KnownType> t2t = new HashMap<>();

    KnownType(Type boxed, Type unboxed) {
        this.boxed = boxed;
        this.unboxed = unboxed;
    }

    static {
        for (KnownType value : values()) {
            if (value != OBJECT) {
                t2t.put(value.boxed, value);
                t2t.put(value.unboxed, value);
            }
        }
    }

    public static KnownType of(Type type) {
        return t2t.getOrDefault(type, OBJECT);
    }
}
