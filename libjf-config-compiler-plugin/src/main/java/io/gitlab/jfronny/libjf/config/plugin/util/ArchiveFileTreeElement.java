package io.gitlab.jfronny.libjf.config.plugin.util;

import org.gradle.api.file.FileTreeElement;
import org.gradle.api.file.RelativePath;
import org.gradle.api.internal.file.DefaultFileTreeElement;

import java.io.*;

public class ArchiveFileTreeElement implements FileTreeElement {
    private final RelativeArchivePath archivePath;

    public ArchiveFileTreeElement(RelativeArchivePath archivePath) {
        this.archivePath = archivePath;
    }

    public boolean isClassFile() {
        return archivePath.isClassFile();
    }

    @Override
    public File getFile() {
        return null;
    }

    @Override
    public boolean isDirectory() {
        return archivePath.entry.isDirectory();
    }

    @Override
    public long getLastModified() {
        return archivePath.entry.getLastModifiedDate().getTime();
    }

    @Override
    public long getSize() {
        return archivePath.entry.getSize();
    }

    @Override
    public InputStream open() {
        return null;
    }

    @Override
    public void copyTo(OutputStream outputStream) {

    }

    @Override
    public boolean copyTo(File file) {
        return false;
    }

    @Override
    public String getName() {
        return archivePath.getPathString();
    }

    @Override
    public String getPath() {
        return archivePath.getLastName();
    }

    @Override
    public RelativeArchivePath getRelativePath() {
        return archivePath;
    }

    @Override
    public int getMode() {
        return archivePath.entry.getUnixMode();
    }

    public FileTreeElement asFileTreeElement() {
        return new DefaultFileTreeElement(null, new RelativePath(!isDirectory(), archivePath.getSegments()), null, null);
    }
}
