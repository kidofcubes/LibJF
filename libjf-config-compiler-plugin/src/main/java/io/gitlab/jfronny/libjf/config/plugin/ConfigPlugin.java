package io.gitlab.jfronny.libjf.config.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.BasePluginExtension;
import org.gradle.api.tasks.bundling.Jar;

import java.io.File;

public class ConfigPlugin implements Plugin<Project> {
    @Override
    public void apply(Project project) {
        project.getTasks().register("injectCompiledConfig", ConfigInjectTask.class, task -> {
            task.from(project.getTasks().named("jar", Jar.class));
            task.getModId().set(project.getExtensions().getByType(BasePluginExtension.class).getArchivesName());
            task.getArchiveClassifier().set("config-inject");
            task.getDestinationDirectory().set(new File(project.getBuildDir(), "devlibs"));
        });
    }
}
