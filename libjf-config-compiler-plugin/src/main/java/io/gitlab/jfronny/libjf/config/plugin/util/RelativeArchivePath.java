package io.gitlab.jfronny.libjf.config.plugin.util;

import org.apache.tools.zip.ZipEntry;
import org.gradle.api.file.RelativePath;

import java.util.Arrays;
import java.util.List;

public class RelativeArchivePath extends RelativePath {
    public ZipEntry entry;

    public RelativeArchivePath(ZipEntry entry) {
        super(!entry.isDirectory(), entry.getName().split("/"));
        this.entry = entry;
    }

    public boolean isClassFile() {
        return getLastName().endsWith(".class");
    }

    @Override
    public RelativeArchivePath getParent() {
        List<String> segments = Arrays.asList(getSegments());
        if (segments.size() == 1) {
            return null;
        } else {
            //Parent is always a directory so add / to the end of the path
            String path = String.join("/", segments.subList(0, segments.size() - 1)) + "/";
            return new RelativeArchivePath(new ZipEntry(path));
        }
    }
}
