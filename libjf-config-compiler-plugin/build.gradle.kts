import io.gitlab.jfronny.scripts.*
import javax.lang.model.element.Modifier.*

plugins {
    id("java-gradle-plugin")
    id("jf.maven-publish")
    id("jf.codegen")
}

group = rootProject.group
version = rootProject.version

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
    maven("https://maven.fabricmc.net/")
}

dependencies {
    compileOnly(gradleApi())
    implementation("org.apache.ant:ant:${prop("ant_version")}")
    implementation("io.gitlab.jfronny:commons-gson:${prop("commons_version")}")
    implementation("org.ow2.asm:asm:${prop("asm_version")}")
    implementation("org.ow2.asm:asm-commons:${prop("asm_version")}")
    implementation(project(":libjf-config-core-v1")) { isTransitive = false }
}

gradlePlugin {
    plugins {
        create("simplePlugin") {
            id = "io.gitlab.jfronny.libjf.libjf-config-compiler-plugin"
            implementationClass = "io.gitlab.jfronny.libjf.config.plugin.ConfigPlugin"
        }
    }
}

tasks.publish.get().dependsOn(tasks.build.get())
rootProject.tasks.deployDebug.dependsOn(tasks.publish.get())

sourceSets {
    main {
        generate(project) {
            `class`("io.gitlab.jfronny.libjf.config.plugin", "BuildMetadata") {
                modifiers(PUBLIC)
                field("IS_RELEASE", project.hasProperty("release"), PUBLIC, STATIC, FINAL)
            }
        }
    }
}