package io.gitlab.jfronny.libjf.config.plugin;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;

import javax.lang.model.element.TypeElement;

public record ConfigClass(TypeElement classElement, ClassName className, TypeName typeName, ClassName generatedClassName, String[] referencedConfigs) {
    public static ConfigClass of(TypeElement element, String[] referencedConfigs, boolean hasManifold) {
        ClassName className = ClassName.get(element);
        String pkg = hasManifold ? "gsoncompile.extensions." + className.packageName() + "." + className.simpleNames().get(0) : className.packageName();
        ClassName generatedClassName = ClassName.get(pkg, "JFC_" + className.simpleNames().get(0), className.simpleNames().subList(1, className.simpleNames().size()).toArray(String[]::new));
        return new ConfigClass(element, ClassName.get(element), TypeName.get(element.asType()), generatedClassName, referencedConfigs);
    }
}
