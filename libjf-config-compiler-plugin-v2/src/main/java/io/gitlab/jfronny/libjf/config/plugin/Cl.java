package io.gitlab.jfronny.libjf.config.plugin;

import com.squareup.javapoet.ClassName;

public class Cl {

    public static final ClassName MANIFOLD_EXTENSION = ClassName.get("manifold.ext.rt.api", "Extension");
    public static final ClassName MANIFOLD_THIS = ClassName.get("manifold.ext.rt.api", "This");
}
