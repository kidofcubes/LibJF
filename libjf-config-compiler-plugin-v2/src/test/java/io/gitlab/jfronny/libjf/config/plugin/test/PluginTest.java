package io.gitlab.jfronny.libjf.config.plugin.test;

import io.gitlab.jfronny.libjf.config.api.v1.*;

import java.util.Map;

@JfConfig
public class PluginTest {
    @Entry public static String yes;
    @Entry(min = 12, max = 15) public static double ae;
    @Entry public static byte aByte = 12;
    @Entry public static short aShort = 15;
    @Entry public static char ch = 12;
    @Entry public static int anInt = 64;
    @Entry public static long lonk = 16;
    @Entry public static float f = 15.3f;

    @Category
    public class ExampleCategory {
        @Entry public static boolean innerValue;
        @Entry public static SomeEnum eeAA;
        @Entry public static Map<String, String> sheh;
    }

    @Verifier
    public static void verify() {

    }

    @Preset
    public static void applyMeme() {

    }

    enum SomeEnum {
        Some, Entries
    }
}
