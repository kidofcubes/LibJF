import io.gitlab.jfronny.scripts.*
import javax.lang.model.element.Modifier.*

plugins {
    `java-library`
    id("jf.maven-publish")
    id("jf.codegen")
}

group = rootProject.group
version = rootProject.version

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
    maven("https://maven.fabricmc.net/")
}

dependencies {
    implementation("io.gitlab.jfronny.gson:gson-compile-processor-core:${prop("gson_compile_version")}")
    implementation(devProject(":libjf-config-core-v1"))
    implementation("org.jetbrains:annotations:23.0.0")
    implementation("io.gitlab.jfronny:commons:${prop("commons_version")}")
    implementation("io.gitlab.jfronny:commons-gson:${prop("commons_version")}")
    implementation("com.squareup:javapoet:1.13.0")
    testAnnotationProcessor(sourceSets.main.get().output)
    configurations.testAnnotationProcessor.get().extendsFrom(configurations.implementation.get())
}

tasks.publish.get().dependsOn(tasks.build.get())
rootProject.tasks.deployDebug.dependsOn(tasks.publish.get())

sourceSets {
    main {
        generate(project) {
            `class`("io.gitlab.jfronny.libjf.config.plugin", "BuildMetadata") {
                modifiers(PUBLIC)
                field("IS_RELEASE", project.hasProperty("release"), PUBLIC, STATIC, FINAL)
            }
        }
    }
}

tasks.named<JavaCompile>("compileTestJava") {
    options.compilerArgs.add("-AmodId=example-mod")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}